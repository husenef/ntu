<div class="modal fade" id="ProfileEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method='POST' action='/dashboard'>
                <input type="hidden" name="id_us" value="{{@SESSION.user.user_id}}" />
                <input type="hidden" name="username" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'user_login')}}" />
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Ubah Profil</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label" for="pass">Nama Depan</label>
                        <input id="pass" type="text" class="form-control" name="fname" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'name')}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pass">Nama Belakang</label>
                        <input id="pass" type="text" class="form-control" name="lname" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'last_name')}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pass">{{@tag_mail}}</label>
                        <input id="pass" type="email" class="form-control" placeholder="{{@tag_mail}}" required name="email" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'mail')}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="address">Alamat</label>
                        <input id="address" type="text" class="form-control" placeholder="Alamat" name="address" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'address')}}">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="phone">Telephone</label>
                        <input id="phone" type="text" class="form-control" placeholder="Telephone" required name="telephone" value="{{@obj_users->get_user('id',@SESSION.user.user_id,'telephone')}}">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" value="=1" class="btn btn-success">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>