<?php

namespace controllers;

class Register {

    function register($f3) {
        $obj_ewallets = new \models\Ewallets;
        $f3->set('obj_config', new \models\config());
        $f3->set('arr_ewallets', $obj_ewallets->get_ewallets());
        echo \Template::instance()->render('page_register.htm');
    }

    function add_user($f3) {
        $obj_users = new \models\Users;
        $post = $f3->get('POST');
        $f3->scrub($post);
        if (!$post) {
            $f3->reroute('/');
        }

        $id = $obj_users->get_user('user_login', $post['username'], 'id');

        if (!isset($id)) {
            $UID = substr(uniqid(), 7, 7);
            $user_data = array(
                'code' => $UID,
                'name' => $post['fnama'],
                'last_name' => $post['lnama'],
                'mail' => $post['email'],
                'user_login' => $post['username'],
                'user_password' => md5($post['password']),
                'telephone' => $post['telephone'],
                'ewallet_login' => $post['ewallet_id'],
                'ewallet_type' => 'Adira box',
                'address' => $post['address'],
                'cabang' => (!empty($post['cabang'])) ? $post['cabang'] : 0,
                'role' => 0
            );
//            print_r($user_data);
//            exit();
            $obj_users->add_user($user_data);

            $this->send_email_confirm($post['email'], $post['username'], $post['password'], md5($UID));
            $this->send_admin_notify_email($post['fnama'], $post['lnama']);
            $f3->reroute('/?notification=info.acoount');
        } else {
            $f3->reroute('/register/?notification=error.exist');
        }
    }

    function lost_pass($f3) {
        $post = $f3->get('POST');
        $f3->scrub($post);

        if ($f3->get('POST.username') && $f3->get('POST.lastname') && $f3->get('POST.email')) {

            $obj_users = new \models\Users;
            $data = $obj_users->get_user_lost_pass($f3->get('POST.username'), $f3->get('POST.lastname'), $f3->get('POST.email'));

            if ($data) {

                $this->send_email_lost($data->mail, $data->user_login, $data->code);
                $f3->set('notification', 'info');
            } else {
                $f3->set('notification', 'error');
            }
        }

        echo \Template::instance()->render('page_lost_pass.htm');
    }

    function confirm_email($f3) {
        $obj_users = new \models\Users;
        $get = $f3->get('GET');
        //$f3->scrub($get);

        if (isset($get['user_name'])) {

            $cd = $obj_users->get_user('user_login', $get['user_name'], 'code');

            if (md5($cd) == $get['user_code'])
                $obj_users->confirm_user($get['user_name']);
        }
        $f3->reroute('/');
    }

    function send_email_confirm($user_mail, $user_name, $user_pass, $user_code) {
        $obj_config = new \models\config();
        $confirm_link = (\Base::instance()->get('ROOT') . '/register/confirm?user_name=' . $user_name . '&user_code=' . $user_code);

        $title = $obj_config->get_config_field_value('value', 'confirm_mail_title', 'data');
        $subject = $obj_config->get_config_field_value('value', 'confirm_mail_subject', 'data');
        $message = $obj_config->get_config_field_value('value', 'confirm_mail_message', 'data');
        $from = $obj_config->get_config_field_value('value', 'confirm_mail_from', 'data');
        $fromsrc = $obj_config->get_config_field_value('value', 'confirm_mail_from_src', 'data');

        $f3 = \Base::instance();
        $tag_uname = $f3->get('tag_username');
        $tag_pass = $f3->get('tag_password');

        $message = "
            
            <html>
                <head>
                  <title>$title</title>
                </head>
                <body>
                  <p>$message</p>
                  <p><b>$tag_uname: </b>$user_name; <b>$tag_pass: </b>$user_pass</p>
                  <a href='$confirm_link'>$confirm_link</a>
                </body>
            </html>
                        
                    ";
        $to = $user_mail;

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= "To: $user_name <$to>" . "\r\n";
        $headers .= "From: $from <$fromsrc>" . "\r\n";

        mail($to, $subject, $message, $headers);
    }

    function send_admin_notify_email($user_name, $user_lastname) {
        $obj_config = new \models\config();

        $f3 = \Base::instance();
        $tag_uname = $f3->get('tag_username');
        $tag_msg = $f3->get('admin_mail_not_msg');

        $title = $f3->get('admin_mail_not_rg');
        $subject = $f3->get('admin_mail_not_nr');
        $from = $obj_config->get_config_field_value('value', 'title', 'data');
        $fromsrc = \Base::instance()->get('ROOT');



        $message = "
            
            <html>
                <head>
                  <title>$title</title>
                </head>
                <body>
                  <p>$tag_msg</p>
                  <p><b>$tag_uname: </b>$user_name $user_lastname;</p>
                </body>
            </html>
                        
                    ";
        $to = $obj_config->get_config_field_value('value', 'admin_mail_dir', 'data');

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= "To: admin <$to>" . "\r\n";
        $headers .= "From: $from <$fromsrc>" . "\r\n";

        mail($to, $subject, $message, $headers);
    }

    function send_email_lost($user_mail, $user_name, $user_code) {
        $f3 = \Base::instance();
        $tag_uname = $f3->get('tag_username');
        $tag_pass = $f3->get('tag_password');

        $obj_config = new \models\config();
        $title = $f3->get('admin_mail_lost_title');
//        $subject = $f3->get('admin_mail_lost_sub');
//        $messageo = $f3->get('admin_mail_lost_msg');
        $subject = $obj_config->get_config_field_value('value', 'admin_mail_lost_sub', 'data');
        $messageo = $obj_config->get_config_field_value('value', 'admin_mail_lost_msg', 'data');
        $from = $obj_config->get_config_field_value('value', 'confirm_mail_from', 'data');
        $fromsrc = $obj_config->get_config_field_value('value', 'confirm_mail_from_src', 'data');
        $messageo = str_replace('[password]', $user_code, $messageo);
        $messageo = str_replace('[username]', $user_name, $messageo);

//        $message = "
//            
//            <html>
//                <head>
//                  <title>$title</title>
//                </head>
//                <body>
//                  <p>$message</p>
//                  <p><b>$tag_uname: </b>$user_name; <b>$tag_pass: </b>$user_code</p>
//                </body>
//            </html>
//                        
//                    ";
        $message = "<html>
                <head>
                  <title>$title</title>
                </head>
                <body>
                  $messageo
                </body>
            </html>";
        $to = $user_mail;

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= "To: $user_name <$to>" . "\r\n";
        $headers .= "From: $from <$fromsrc>" . "\r\n";
        mail($to, $subject, $message, $headers);
    }

}
