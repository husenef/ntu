<?php

namespace controllers;

class User_view {

    function beforeroute($f3) {
        if ($f3->get('INSTALLED') == 'TRUE') {
            $security = new Security();
            if ($security->check_login()) {
                if ($f3->get('SESSION.user.role') == 1)
                    $f3->reroute('/admin');
            }elseif ($security->check_login()) {
                if ($f3->get('SESSION.user.role') == 2) {
                    if (!empty($session['goto'])) {
                        $target = $session['goto'];
                        $f3->clear('SESSION.goto');
                        $f3->reroute($target);
                    } else {
                        $f3->reroute('/cabang');
                    }
                }
            } else {
                if ($f3->get('GET.notification'))
                    $f3->reroute('/?notification=' . $f3->get('GET.notification'));
                else {
                    if ($f3->get('SESSION.user') && $f3->get('SESSION.user.is_mail_confirm') == 0)
                        $f3->reroute('/?notification=info.acoount');
                }
            }
        }else {
            $f3->reroute('/install');
        }
    }

    function user_dashboard($f3) {
        $post = $f3->get('POST');
        $get = $f3->get('GET');
        $session = $f3->get('SESSION.user');
        $root = $f3->get('ROOT');
        $cookie = $f3->get('COOKIE');

//redirect to user role
        if ($f3->get('SESSION.user.role') == 2) {
            if (!empty($session['goto'])) {
                $target = $session['goto'];
                $f3->clear('SESSION.goto');
                $f3->reroute($target);
            } else {
                $f3->reroute('/cabang');
            }
        }
        if (!$f3->get('SESSION.user'))
            $f3->reroute('/');

        $obj_survei = new \models\Survei();
        $obj_clikc = new \models\Click();
        $obj_produk = new \models\Produk();

        $obj_user = new \models\Users();
        $obj_cabang = new \models\Substation();

        $limit = $f3->get('PAGINATION');
        $count = $obj_survei->get_count_data(array("id_agen=? ", $session['user_id']));
        $pages = new \Pagination($count, $limit);
        $f3->set('obj_users', $obj_user);
        $f3->set('show_pagination', true);
        $f3->set('pagebrowser', $pages->serve());
        $f3->set('no', $pages->getItemOffset());

        $srv = $obj_survei->get_surveis(array("id_agen=? ", $session['user_id']), array('order' => 'date_save DESC', 'limit' => $limit, 'offset' => $pages->getItemOffset()));

        if ($post && is_numeric($post['id_us'])) {
            $obj_user = new \models\Users();
            $data = array(
                'name' => $post['fname'],
                'last_name' => $post['lname'],
                'mail' => $post['email'],
                'address' => $post['address'],
                'telephone' => $post['telephone']
            );
            $obj_user->upd_user($post['id_us'], $data);
            $f3->reroute('/dashboard?notification=success.update');
        }
//        $f3->set('no', '0');
        $f3->set('srv', $srv);
        $f3->set('obj_produk', new \models\Produk());
        $f3->set('obj_kategori', new \models\Kategori());
        $f3->set('obj_survei', new \models\Survei());
        $f3->set('c_survei', new \controllers\Survei());
        $f3->set('content', 'users/user_dashboard.htm');
        echo \Template::instance()->render('user_index.htm');
    }

    function agen($f3) {
        if ($f3->get('SESSION.user.role') == 0)
            $f3->reroute('/');
        $post = $f3->get('POST');
        $get = $f3->get('GET');

        $obj_survei = new \models\Survei();
        $obj_user = new \models\Users();

        if ($post) {
            $obj_survei->update_status($post['id'], $post['update']);
        }

        $cabang = $obj_user->get_user('id', $f3->get('SESSION.user.user_id'), 'cabang');
        $option = array('order' => 'date_save DESC');
        if (!empty($get['order'])) {
            switch ($get['order']) {
                case 'agen':
                    $option = array('order' => 'id_agen ASC');
                    break;
                case 'tanggal':
                    $option = array('order' => 'date_save');
                    break;
            }
        }
        $survei = $obj_survei->get_surveis(array('id_cabang=? and id_user_cabang=? and enval=?', $cabang, $f3->get('SESSION.user.user_id'), 'A'), $option);
        $ord = (!empty($get['order'])) ? $get['order'] : '';
        $f3->set('ord', $ord);
        $f3->set('survei', $survei);
        $f3->set('obj_users', new \models\Users());
        $f3->set('c_survei', new \controllers\Survei());
        $f3->set('content', 'cabang/cabang_dasboard.htm');
        $f3->set('obj_article', new \models\Article());
        echo \Template::instance()->render('cabang_index.htm');
    }

    function message($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        if ($f3->get('SESSION.user.role') == 0)
            $f3->reroute('/');
        if (empty($get['ms']))
            $f3->reroute('/');
        $id = $get['ms'];
        $obj_survei = new \models\Survei;
        $obj_user = new \models\Users();
        $obj_produk = new \models\Produk();
        $f3->set('obj_users', new \models\Users());
        $f3->set('c_survei', new \controllers\Survei());

        $survei = $obj_survei->get_data($id);

        $id_produk = $survei->id_produk;
        $id_agen = $survei->id_agen;
        $id_user_cabang = $survei->id_user_cabang;



        if (!empty($post['update'])) {
            $obj_survei->update_status($survei->id, array('code' => $post['update'], 'extra_text' => (isset($post['extra_text']) ? $post['extra_text'] : '')));
            $f3->reroute('/message?ms=' . $survei->id);
        }
        $produk = null;
        if (!empty($id_produk)) {
            $produk = $obj_produk->get_produk($id_produk);
        }

        $agen = $obj_user->get_user_by_id($id_agen);

        if (!empty($get['action'])) {
            $obj_category = new \models\Kategori();
            $obj_substation = new \models\Substation();

            $pdf = new \Fpdf\FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 26);
            $pdf->Cell(120, 10, 'Data Pengajuan Kredit');
            $pdf->Ln(10);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(120, 10, 'ADIRA\\' . $obj_substation->get_substation($survei->id_cabang, 'name') . '\\' . date('dmy', strtotime($survei->date_save)) . '\\' . $agen->ewallet_login . '\\' . $survei->id);
            // Line break
            $pdf->Ln(10);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Cell(25, 10, 'Data Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Ln(8);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(40, 10, 'Nama Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->name . ' ' . $agen->last_name);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Alamat Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->address);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No Telepon Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->telephone);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Email Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->mail);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No VA');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->ewallet_login);
            $pdf->Ln(15);
            $pdf->Cell(190, 1, '', 1, '', '', 'yes');
            $pdf->Ln(8);

            /* data nasabag */
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Cell(25, 10, 'Data Nasabah :');
            $pdf->Ln(8);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(40, 10, 'Nama ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei->nama);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No KTP ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei->ktp);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No Telepon ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei->telephone);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Alamat ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei->alamat);
            $pdf->Ln(8);
            $pdf->Ln(15);
            $pdf->Cell(190, 1, '', 1, '', '', 'yes');
            $pdf->Ln(8);
            /* data produk */
            if (!empty($produk)) {
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Cell(25, 10, 'Data Produk :');
                $pdf->Ln(8);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(40, 10, 'Nama Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $produk->produk . '(' . $obj_category->get_kategori_field($produk->merek, 'nama') . ')');
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Jenis Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $obj_category->get_jenis($survei->jenis_produk));
            } else {
                $pdf->Cell(40, 10, 'Jenis Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $obj_category->get_jenis($survei->jenis_produk));
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Jumlah Maxi');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, 'Rp' . number_format($survei->dana, 0, '', '.') . ',-');
            }
            $pdf->Ln(8);
            $pdf->Cell(190, 0.5, '', 1, '', '', 'yes');
            $pdf->Ln();
            $pdf->Cell(85, 10, 'Nama CRH yang bertanggung jawab');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $obj_user->get_user('id', $id_user_cabang, 'name') . ' ' . $obj_user->get_user('id', $id_user_cabang, 'last_name'));
            $pdf->Ln(8);
            $pdf->Cell(15, 10, 'Pesan');
            $pdf->Cell(5, 10, ':');
            $pdf->Ln(10);

            $pdf->MultiCell(190, 8, $survei->more_text);
            $pdf->Ln(5);

            $pdf->Output('ADIRA-' . $agen->ewallet_login . "-" . str_replace(" ", "-", $obj_substation->get_substation($survei->id_cabang, 'name')) . '-' . $survei->id . '.pdf', 'D');
//            $pdf->Output();
        } else {
            $f3->set('survei', $survei);
            $f3->set('agen', $agen);
            $f3->set('produk', $produk);
            $f3->set('obj_kategori', new \models\Kategori());
            $f3->set('content', 'cabang/cabang_message.htm');
            echo \Template::instance()->render('cabang_index.htm');
        }
    }

    function contact_us($f3) {
        $post = $f3->get('POST');
        $obj_config = new \models\config();
        if ($post) {
            $message = "
            
            <html>
                <head>
                  <title>Anda mendapat pesan </title>
                </head>
                <body>
                  <p>hai Admin,anda mendapat pesan dari:<br/>
                  nama: " . $post['nama'] . "<br/>
                      email: " . $post['email'] . "<br/>
                          telepone : " . $post['phone'] . "<br/>
                              Nomer Adira Finance Club : " . ($post['nomor']) ? $post['nomor'] : '-' . "<br/>
                                  Pesan:<br/>
                                  <quote>" . $post['pertanyaan'] . "</quote>
</p>
<p>Terimakasih</p>
                </body>
            </html>
                        
                    ";
            $subject = "Email dari contact us";
            $to = $obj_config->get_config_field_value('value', 'tnc', 'data');
            $user_name = 'admin';

            // To send HTML mail, the Content-type header must be set
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

            // Additional headers
            $headers .= "To: $user_name <$to> " . "\r\n";
            $headers .= "From: " . $post['nama'] . " <" . $post['email'] . ">" . "\r\n";

            mail($to, $subject, $message, $headers);
            $f3->reroute('/hubungi?s=e');
        }
        $f3->set('obj_users', new \models\Users());
        $f3->set('content', 'users/contact_us.htm');
        echo \Template::instance()->render('user_index.htm');
    }

    function nasabah_user_edit_profile($f3) {
        $session = $f3->get('SESSION');
        $post = $f3->get('POST');
        $file = $f3->get('FILES');
        $f3->set('obj_users', new \models\Users());
        if (empty($session['user']))
            $f3->reroute('/');
        if ($post) {
            $obj_user = new \models\Users();
            $data = array(
                'name' => $post['fnama'],
                'last_name' => $post['lnama'],
                'mail' => $post['email'],
                'telephone' => $post['telephone'],
                'address' => $post['address']
            );
            $id = ($session['user']['user_id']);

            if ($file['foto']['size']) {
//                print_r($file);
                $name = $id . '_' . md5(date(DATE_RSS)) . '.jpg';
                $destination = './content/user/';
                move_uploaded_file($file['foto']['tmp_name'], $destination . $name);
                if (!empty($post['oldp'])) {
                    if (file_exists($destination . $post['oldp'])) {
                        unlink($destination . $post['oldp']);
                    }
                }
                $data['img'] = $name;
            }
            
            $obj_user->upd_user($id, $data);
            $f3->reroute('/profile/edit?ckr=sor');
        }

        if ($session['user']['role'] == 2) {
            $f3->set('content', 'cabang/edit_profile.htm');
            echo \Template::instance()->render('cabang_index.htm');
        } else {
            $f3->set('content', 'users/edit_profile.htm');
            echo \Template::instance()->render('user_index.htm');
        }
    }

    function nasabah_user_edit_password($f3) {
        $session = $f3->get('SESSION');
        $post = $f3->get('POST');
        $f3->set('obj_users', new \models\Users());
        if (empty($session['user']))
            $f3->reroute('/');
        $us = array();
        if ($post) {
            $obj_user = new \models\Users();
            $us = $obj_user->get_users_by_role(array('id=?', $session['user']['user_id']));

            if ($us[0]->user_password == md5($post['curent'])) {
                $obj_user->upd_admin_pass($session['user']['user_id'], $post['new']);
                $er = $obj_user->get_users_by_role(array('id=?', $session['user']['user_id']));

                $f3->reroute('/profile/ganti_password?s=succes');
            } else {
                $f3->reroute('/profile/ganti_password?s=error');
            }
        }

        if ($session['user']['role'] == 2) {
            $f3->set('content', 'cabang/edit_password.htm');
            echo \Template::instance()->render('cabang_index.htm');
        } else {
            $f3->set('content', 'users/edit_password.htm');
            echo \Template::instance()->render('user_index.htm');
        }
    }

}
