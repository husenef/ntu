<?php

namespace controllers;

class Administration {

    function beforeroute($f3) {
        if ($f3->get('INSTALLED') == 'TRUE') {
            $security = new Security();
            if ($security->check_login()) {
                if ($f3->get('SESSION.user.role') == 0)
                    $f3->reroute('/user');
                if ($f3->get('SESSION.user.role') == 2)
                    $f3->reroute('/cabang');
            }else
            if ($f3->get('GET.notification')) {
//                $f3->reroute('/login?notification=' . $f3->get('GET.notification'));
                $f3->reroute('/?notification=' . $f3->get('GET.notification'));
            } else {
                $f3->reroute('/');
            }
        } else {
            $f3->reroute('/install');
        }
    }

    function dashboard($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $f3->scrub($post);
        $f3->scrub($get);

        $m_survei = new \models\Survei();
        $count = $m_survei->survei_all(array('id_agen !=?', 0), array('group' => 'id_agen'));
        $count = (count($count));

        $mod_log = new \models\Log();
        $db = $f3->get('DB');
        $q = $mod_log->query_search(5);
        $result_log = $db->exec($q);

        $f3->set('obj_users', new \models\Users());
        $f3->set('obj_produk', new \models\Produk());
        $f3->set('obj_survei', new \models\Survei());
        $f3->set('result_total', $count);
        $f3->set('resultLog', $result_log);
        $f3->set('content', 'admin/admin_dashboard.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function users($f3, $params) {
        if (empty($params['code']))
            $f3->reroute('/');

        switch ($params['code']) {
            case 'agen':
                $role = 0;
                $url = 'agen';
                break;

            case'cabang':
                $role = 2;
                $url = 'cabang';
                break;
        }
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $files = $f3->get('FILES');
        $session = $f3->get('SESSION');
        $userlistinsert = '';
        if (isset($session['userlist'])) {
            $userlistinsert = $session['userlist'];
            $f3->clear('SESSION.userlist');
//            echo "<pre>";
//            print_r($userlistinsert);
//            echo "</pre>";
//            die();
        }
        $obj_user = new \models\Users();
        $obj_cabang = new \models\Substation();
        $return = array();
        if ($files) {
            $this->user_upload($f3, $files, $params['code']);
        }

        $limit = $f3->get('PAGINATION');
        $f3->set('limit', $limit);
        $count = $obj_user->count(array('role=? and active=?', $role, '1'));
        $pages = new \Pagination($count, $limit);
        $offset = $pages->getItemOffset();
        $f3->set('pagebrowser', $pages->serve());
        $f3->set('offset', $offset);

        if (isset($get['action']) && $get['action'] == 'delete') {
            $obj_user->del_user($get['item']);
            $f3->reroute("/admin/users/$url?notification=delete.s");
        }
        if (isset($get['action']) && $get['action'] == 'update') {
//            $login_user = $obj_user->get_user('id', $get['item'], 'user_login');
//            print_r($login_user);
            $obj_user->confirm_user($get['item']);
        }

        //save user
        if ($post) {
//delete all
            if (!empty($post['all_id'])) {
                for ($index = 0; $index < count($post['all_id']); $index++) {
                    $obj_user->del_user($post['all_id'][$index]);
                    $obj_user->reset();
                }
                echo 'y';
                exit();
            }

            if (!empty($post['data_user'])):
                if (empty($post['ewallet_id'])) {
                    $user_pass = array();
                    if (($post['password']) != '')
                        $user_pass = array('user_password' => $post['password']);
                    $user_data = array(
                        'name' => $post['fnama'],
                        'last_name' => $post['lnama'],
                        'mail' => $post['email'],
                        'telephone' => $post['telephone'],
//                        'ewallet_login' => (!empty($post['ewallet_id'])) ? $post['ewallet_id'] : 0,
                        'user_login' => $post['username'],
                        'ewallet_type' => 'Adira box',
                        'address' => (!empty($post['address'])) ? $post['address'] : null,
                        'cabang' => (!empty($post['cabang'])) ? $post['cabang'] : '',
                        'nik' => (!empty($post['nik'])) ? $post['nik'] : 0,
                        'role' => $role,
                        'mail_confirm' => 1,
                        'active' => 1,
                        'status' => (empty($post['status_user']) ? 0 : $post['status_user'])
                    );

                    $user_data = array_merge($user_data, $user_pass);
                    $obj_user->upd_user($post['iduser'], $user_data);
                    $f3->reroute("/admin/users/$url?notification=succes.update");
                } else {
                    $id = $obj_user->get_user('user_login', $post['username'], 'id');
                    if (!isset($id)) {
                        $UID = substr(uniqid(), 7, 7);

                        $user_data = array(
                            'code' => $UID,
                            'name' => $post['fnama'],
                            'last_name' => $post['lnama'],
                            'mail' => $post['email'],
                            'user_login' => $post['username'],
                            'user_password' => md5($post['password']),
                            'telephone' => $post['telephone'],
                            'ewallet_login' => (!empty($post['ewallet_id'])) ? $post['ewallet_id'] : 0,
                            'ewallet_type' => 'Adira box',
                            'address' => (!empty($post['address'])) ? $post['address'] : null,
                            'cabang' => (!empty($post['cabang'])) ? $post['cabang'] : 0,
                            'nik' => (!empty($post['nik'])) ? $post['nik'] : 0,
                            'role' => $role,
                            'mail_confirm' => 1,
                            'active' => 1,
                            'status' => (empty($post['status_user']) ? 0 : $post['status_user'])
                        );
//                   
                        //chek 
                        $return = $obj_user->get_user('ewallet_login', $user_data['ewallet_login'], 'id');
                        if ($return) {
                            $f3->reroute("/admin/users/$url?notification=already.add");
                        } else {
                            $obj_user->add_user($user_data);
                            $f3->reroute("/admin/users/$url?notification=succes.add");
                        }
                    } else {

                        $f3->reroute("/admin/users/$url?notification=error.exist");
                    }
                }
            endif;
        }
        $cabang = $obj_cabang->get_substations();
        $order = 'date DESC';
        if (!empty($get['by'])) {
            $order = $get['by'];
        }
        $f3->set('searchby', (isset($post['searchby'])) ? $post['searchby'] : '');

        if (!empty($post['s'])) {

            $s = $post['s'];
            $f3->set('s', $s);
            $s = strtolower($s);
//            $user_list = $obj_user->get_users_by_role(array("role = ? AND `name` LIKE '%?%' OR last_name LIKE '%?%' AND active = '?'", $role, $s, $s, '1'));
            switch ($post['searchby']) {
                case 'cabang':
                    $cabangs = $obj_cabang->get_substations(" lower(name) LIKE '%$s%'");
                    foreach ($cabangs as $cb) {
                        $idCab[] = $cb->id;
                    }
                    $ids = implode(',', $idCab);
                    $qs = "role = $role AND cabang in($ids)";
                    break;

                default:
                    //SELECT * FROM `users` WHERE role = 2 AND ( lower(concat(trim(name),' ',trim(last_name))) like '%burhan adi gunawan%' OR nik LIKE '%burhan adi gunawan%') AND active= '1'
                    if ($role == 0)
                        $qs = "role = $role AND ( lower(concat(trim(name),' ',trim(last_name))) LIKE '%$s%' OR ewallet_login LIKE '%$s%') AND active = '1'";
                    else
                        $qs = "role = $role AND ( lower(concat(trim(name),' ',trim(last_name))) LIKE '%$s%' OR nik LIKE '%$s%') AND active = '1'";
                    break;
            }
//            die($qs);
            $user_list = $obj_user->select("*", "$qs");
            
            $f3->set('show_pagination', false);
        } else {
            $f3->set('show_pagination', true);
            $user_list = $obj_user->get_users_by_role(array('role=? and active=? ', $role, '1'), array('order' => $order, 'limit' => $limit, 'offset' => $offset));
        }

        $notif = null;

        if (isset($session['notification']['message'])) {
            $notif = $session['notification']['message'];
            $f3->set('SESSION.notification', array('message' => null, 'in' => 0));
        }

        $f3->set('notif', $notif);
        $f3->set('no', 1);
        $f3->set('url', $url);
        $f3->set('role', $role);
        $f3->set('userlistinsert', $userlistinsert);
        $f3->set('cabang', $cabang);
        $f3->set('user_list', $user_list);

        $f3->set('obj_users', new \models\Users());
        $f3->set('obj_cabang', new \models\Substation());
        $f3->set('content', 'admin/admin_users.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function user_upload($f3, $files, $code) {
        switch ($code) {
            case 'agen':
                $role = 0;
                $url = 'agen';
                break;

            case'cabang':
                $role = 2;
                $url = 'cabang';
                break;
        }
        $obj_user = new \models\Users();

        if (!preg_match('/csv/i', $files['data_user']['name'])) {
            $f3->reroute("/admin/users/$url?notification=error.add.more");
            exit();
        }

        $file_name = $files['data_user']['name'];

        $upload = move_uploaded_file($files['data_user']['tmp_name'], './assets/csv/' . $file_name);
        if ($upload) {
            $row = 0;
            $g = false;
            if (($handle = fopen("./assets/csv/$file_name", "r")) !== FALSE) {
                $user_datas = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);

//                    if ($row > 0):
                    if ($row > 0) {
                        //if agen
                        $UID = substr(uniqid(), 7, 7);
                        if ($role == 0) {
                            $user_datas = array(
                                'code' => $UID,
                                'name' => $data['0'],
                                'last_name' => $data['1'],
                                'mail' => $data['2'],
                                'user_login' => $data['6'],
                                'user_password' => md5($data['6']),
                                'telephone' => $data['5'],
                                'ewallet_login' => $data['6'],
                                'ewallet_type' => $data['3'],
                                'address' => $data['4'],
                                'cabang' => 0,
                                'role' => 0,
                                'mail_confirm' => 1,
                                'active' => 1
                            );
                            //check agent
//                            $check = $obj_user->get_user('ewallet_login', $user_datas['ewallet_login'], 'id');
                            $check1 = $obj_user->user_by_load(array('ewallet_login=? and active=?', $user_datas['ewallet_login'], '1'));

                            $check = ($check1) ? $check1['id'] : '';
                        } elseif ($role == 2) {
                            $user_datas = array(
                                'code' => $UID,
                                'name' => $data['0'],
                                'last_name' => $data['1'],
                                'mail' => $data['2'],
                                'user_login' => $data['5'],
                                'user_password' => md5($data['5']),
                                'telephone' => '0' . $data['4'],
                                'ewallet_login' => ($data['6']),
                                'ewallet_type' => 'Adira box',
                                'address' => (!empty($post['address'])) ? $post['address'] : null,
                                'cabang' => (!empty($data['3'])) ? $data['3'] : 0,
                                'nik' => (!empty($data['5'])) ? $data['5'] : 0,
                                'role' => $role,
                                'status' => $data['7'],
                                'mail_confirm' => 1,
                                'active' => 1
                            );
                            //check agent
//                            $check = $obj_user->get_user('user_login', $user_datas['user_login'], 'id');
                            $check1 = $obj_user->user_by_load(array('ewallet_login=? and active=?', $user_datas['ewallet_login'], '1'));
                            $check = ($check1) ? $check1['id'] : '';
                        }

                        if (!$check) {
                            $g = $obj_user->add_user($user_datas);
//                                $obj_user->reset();
//                                $row++;
                        } else {
                            $userId = ($role == 0) ? $user_datas['ewallet_login'] : $user_datas['user_login'];
                            $active = ( $check1['active'] == 1) ? 'active' : '';
                            $users[] = $userId . ' Nama:' . $check1['name'] . ' ' . $check1['last_name'] . ' status:' . $active;
                        }
                    }
//                    endif;
                    $row++;
                }
                fclose($handle);
            }
            $f3->set('SESSION.notification', array('message' => 'succes.add.more', 'in' => $row));
            $f3->set('SESSION.userlist', $users);
            unlink("./assets/csv/$file_name");
            $f3->reroute("/admin/users/$url");
        } else {
            print_r('Error upload');
        }
        exit();
    }

    function brokers($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $files = $f3->get('FILES');

        //$f3->scrub($post);
        $f3->scrub($get);

        $obj_produk = new \models\Produk();
        $obj_click = new \models\Click();

        if (isset($post['produk'])) {
            $imgArray = array('image/jpeg', 'image/png', 'image/gif');
            if (isset($files) && (in_array($files['produk_file']['type'], $imgArray))) {
                $filename = md5(date('H:i:s d-m-y')) . '.jpg';
                move_uploaded_file($files['produk_file']['tmp_name'], './assets/images/' . $filename);
                $post['foto'] = $filename;
            }
            $obj_produk->add_produk($post);
        }

        if (isset($get['action']) && $get['action'] == 'delete') {
            $obj_produk->del_produk($get['item']);
        }
        $allproduk = $obj_produk->get_produks();
        $dklik = array();
        foreach ($allproduk as $ey => $vle) {
            $dklik[$ey]['diklik'] = $obj_click->check_count_by_produk($vle->id);
        }

        $f3->set('dklik', $dklik);
        $f3->set('allproduk', $allproduk);
        $f3->set('content', 'page_admin_brokers.htm');
        echo \Template::instance()->render('__layout.htm');
    }

    function ewallets($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $f3->scrub($post);
        $f3->scrub($get);

        if (isset($post['ewallet']) && isset($post['ewallet_link'])) {
            $obj_ewallets = new \models\Ewallets();

            if ($post['id'] == '')
                $obj_ewallets->add_ewallet($post);
            else
                $obj_ewallets->upd_ewallet($post['id'], $post);
        }

        if (isset($get['action']) && $get['action'] == 'delete') {
            $obj_ewallets = new \models\Ewallets();
            $obj_ewallets->del_ewallet($get['item']);
        }

//        $f3->set('content', 'page_admin_wallets.htm');
//        echo \Template::instance()->render('__layout.htm');
        $f3->set('obj_users', new \models\Users());
        $f3->set('obj_ewallets', new models\Ewallets);
        $f3->set('content', 'admin/admin_wallets.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function config($f3) {
        $get = $f3->get('GET');

        $f3->scrub($get);
        $file = $f3->get('FILES');

        if ($f3->get('POST') && !$f3->get('POST.password')) {
            $obj_config = new \models\config();
            $mail_tempalte = $f3->get('POST');


            if (empty($mail_tempalte['mail_template_no']) || $mail_tempalte['mail_template_no'] == 'kosong') {
                $obj_config->upd_config('mail_template', 'kosong');
            }

            if (($file['email_template']['size'] > 0)) {
                $destination = './assets/img/';
                $nname = 'email.jpg';
                if ($destination . $nname) {
//                    unlink($destination . $nname);
                }
                move_uploaded_file($file['email_template']['tmp_name'], $destination . $nname);
                $obj_config->upd_config('mail_template', 'ada');
            }


            $obj_config->upd_config('title', $f3->get('POST.title'));
            $obj_config->upd_config('title_em', $f3->get('POST.title_em'));
            $obj_config->upd_config('subtitle', $f3->get('POST.subtitle'));

            $obj_config->upd_config('confirm_mail_title', $f3->get('POST.confirm_mail_title'));
            $obj_config->upd_config('confirm_mail_from', $f3->get('POST.confirm_mail_from'));
            $obj_config->upd_config('confirm_mail_from_src', $f3->get('POST.confirm_mail_from_src'));
            $obj_config->upd_config('confirm_mail_subject', $f3->get('POST.confirm_mail_subject'));
            $obj_config->upd_config('confirm_mail_message', $f3->get('POST.confirm_mail_message'));
            $obj_config->upd_config('admin_mail_dir', $f3->get('POST.admin_mail_dir'));
            $obj_config->upd_config('notify_mail_title', $f3->get('POST.notify_mail_title'));
            $obj_config->upd_config('notify_mail_subject', $f3->get('POST.notify_mail_subject'));
            $obj_config->upd_config('notify_mail_msg_1', $f3->get('POST.notify_mail_msg_1'));
            $obj_config->upd_config('notify_mail_msg_2', $f3->get('POST.notify_mail_msg_2'));
            $obj_config->upd_config('notify_mail_msg_3', $f3->get('POST.notify_mail_msg_3'));
            $obj_config->upd_config('notify_mail_msg_4', $f3->get('POST.notify_mail_msg_4'));
            $obj_config->upd_config('tnc', $f3->get('POST.tnc'));
            $obj_config->upd_config('admin_mail_lost_msg', $f3->get('POST.admin_mail_lost_msg'));
            $obj_config->upd_config('admin_mail_lost_sub', $f3->get('POST.admin_mail_lost_sub'));

            if ($f3->get('POST.admin_send_mail'))
                $obj_config->upd_config('admin_send_mail', 'true');
            else
                $obj_config->upd_config('admin_send_mail', 'false');

            $obj_config->upd_config('page_register_text', $f3->get('POST.page_register_text'));
            $obj_config->upd_config('page_user_text', $f3->get('POST.page_user_text'));
        }

        if ($f3->get('POST') && $f3->get('POST.password')) {
            $obj_user = new \models\Users();
            $pass = $obj_user->get_user('id', $f3->get('SESSION.user.user_id'), 'user_password');
            if (md5($f3->get('POST.password_old')) == $pass)
                $obj_user->upd_admin_pass($f3->get('SESSION.user.user_id'), $f3->get('POST.password'));
        }
        $f3->set('obj_config', new \models\config());
        $f3->set('content', 'admin/admin_config.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function send_user_notify_email($user_name, $user_mail, $status_val) {
        $obj_config = new \models\config();

        $title = $obj_config->get_config_field_value('value', 'notify_mail_title', 'data');
        $subject = $obj_config->get_config_field_value('value', 'notify_mail_subject', 'data');
        $from = $obj_config->get_config_field_value('value', 'title', 'data');
        $fromsrc = $obj_config->get_config_field_value('value', 'confirm_mail_from_src', 'data');

        switch ($status_val) {
            case 1:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_1', 'data');
                break;

            case 2:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_2', 'data');
                break;

            case 3:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_3', 'data');
                break;

            case 4:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_4', 'data');
                break;
            case 5:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_1', 'data');
                break;

            case 6:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_2', 'data');
                break;

            case 7:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_3', 'data');
                break;

            case 8:
                $message = $obj_config->get_config_field_value('value', 'notify_mail_msg_4', 'data');
                break;

            default:
                break;
        }


        $message = "
            
            <html>
                <head>
                  <title>$title</title>
                </head>
                <body>
                  <p>$message</p>
                </body>
            </html>
                        
                    ";
        $to = $user_mail;

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers
        $headers .= "To: $user_name <$to>" . "\r\n";
        $headers .= "From: $from <$fromsrc>" . "\r\n";

        mail($to, $subject, $message, $headers);
    }

    function substations($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $f3->scrub($post);
        $f3->scrub($get);
        $files = $f3->get('FILES');

        $obj_substations = new \models\Substation();
        $obj_area = new \models\Area();
        $f3->set('obj_area', new \models\Area());
        $areas = $obj_area->find();


        if ($files) {
            if (!preg_match('/csv/i', $files['data_csv']['name'])) {
                $f3->reroute("/admin/substations?notification=error.add.more");
                exit();
            }

            $file_name = $files['data_csv']['name'];
            move_uploaded_file($files['data_csv']['tmp_name'], './assets/csv/' . $file_name);
            $row = 0;
            $g = false;
            if (($handle = fopen("./assets/csv/$file_name", "r")) !== FALSE) {
                $user_datas = array();
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
//                    echo "<p> $num fields in line $row: <br /></p>\n";
                    if ($row != 0) {
                        for ($c = 0; $c < $num; $c++) {
                            $s = explode(';', $data[$c]);
                            echo '<pre>';
                            print_r($s);
                            echo '</pre>';

                            $data = array(
                                'name' => $s[0],
                                'telephone' => $s[1],
                                'kode' => $s[2],
                                'area' => $s[3]
                            );
                            $obj_substations->add_substation($data);
                            $obj_substations->reset();
                        }
                    }
                    $row++;
                }
                fclose($handle);
            }
            unlink("./assets/csv/$file_name");
            $f3->reroute("/admin/substations?notification=succes.add.more");

            exit();
        }

//update or add
        if ($post) {
            if ($post['id'] != '') {
                $obj_substations->upd_substation($post['id'], $post);
                $f3->reroute('/admin/substations?notification=user.update');
            } else {
                $id = $obj_substations->get_substations(array('name=?', $post['name']));
                if (count($id) == 0) {
                    $obj_substations->add_substation($post);
                    $f3->reroute('/admin/substations?notification=user.add');
                } else {
                    $f3->reroute('/admin/substations?notification=user.ada');
                }
            }
        }
//delete data by id
        if (isset($get['action']) && ($get['action'] == 'delete')) {
            if (($get['item'] > 0) && is_numeric($get['item'])) {
                $del = $obj_substations->del_substation($get['item']);
                $f3->set('delstatus', $del);
            }
            $f3->reroute('/admin/substations');
        }
        //get all data
        $all = $obj_substations->get_substations();
//set data        
        $f3->set('areas', $areas);
        $f3->set('allsubstations', $all);
        $f3->set('content', 'admin/admin_substation.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function area($f3) {
        $post = $f3->get('POST');
        $get = $f3->get('GET');
        $obj_area = new \models\Area();
        if ($post) {
            if (!empty($post['id'])) {
                $obj_area->update_area($post);
                $f3->reroute('/admin/area?notification=succes.update');
            } else {
                $obj_area->add_area($post);
                $f3->reroute('/admin/area?notification=succes.add');
            }
        }
        if (!empty($get['action']) && $get['action'] == 'del') {
            $id = base64_decode("$get[token]");
            $obj_area->delet($id);
        }
        $areas = $obj_area->get_areas();
        $f3->set('areas', $areas);
        $f3->set('content', 'admin/admin_area.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

}
