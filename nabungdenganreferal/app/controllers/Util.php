<?php

namespace controllers;

class Util {

    function replace_chars($data) {
        //replace :
        $s = str_replace(':', '%3A', $data);
        //replace /
        $s = str_replace('/', '%2F', $s);
        //replace ?
        $s = str_replace('?', '%3F', $s);
        //replace &
        $s = str_replace('&', '%26', $s);
        //replace =
        $s = str_replace('=', '%3D', $s);

        return $s;
    }

}
