<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logs
 *
 * @author travolta
 */

namespace controllers;

class Logs {

    function dashboard($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $session = $f3->get('SESSION');

        if (empty($session) || $session['user']['role'] != '1')
            $f3->reroute('/');

        //load model
        $mod_user = new \models\Users();
        $mod_log = new \models\Log();

        $count = $mod_log->count_data();
        $limit = 20;

        $pages = new \pagination($count, $limit);
        $offset = $pages->getItemOffset();
        $db = $f3->get('DB');
        $q = $mod_log->query_search($limit, $offset);
        $result = $db->exec($q);

        $f3->set('pagebrowser', $pages->serve());
        $f3->set('mod_log', $mod_log);
        $f3->set('offset', $offset);
        $f3->set('mod_user', $mod_user);
        $f3->set('result', $result);
        $f3->set('content', 'admin/admin_logs.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

}
