<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Survei
 *
 * @author Agus
 */

namespace controllers;

class Survei {

    //put your code here
    function dashboard($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $session = $f3->get('SESSION');
        if (!isset($session['ref']))
            $f3->reroute('/');

        $referal = $session['ref'];
        //load model

        $obj_clikc = new \models\Click();
        $obj_area = new \models\Area();

        $linkdata = $obj_clikc->cari($referal['link_id']);
        if (!is_object($linkdata) && $linkdata == 0)
            $f3->reroute('/');
        $data = array(
            'agen' => $linkdata['user_id'],
            'link_id' => $linkdata['id']
        );
        $area = $obj_area->get_areas();

        $f3->set('areas', $area);
        $f3->set('user', $data);
        echo \Template::instance()->render('users/user_survei_form.htm');
    }

    function save($f3) {
        $post = $f3->get('POST');
        $obj_survei = new \models\Survei();
        $obj_users = new \models\Users();

        if (!empty($post)) {
            $f3->clear('SESSION.ref');
            /*             * proses ke cabang             */
            /* send email ke cabang */
//            
            switch ($post['used']) {
                case 1:
                    $s = 2;
                    $sub = '(Channeling)';
                    break;
                case 2:
                    $s = 1;
                    $sub = '(Retention)';
                    break;
                default:
                    $s = 3;
                    $sub = '(Channeling & Retention)';
                    break;
            }
            $users = $obj_users->get_users_by_role(array('cabang=? and active=? and is_mail_confirm=? and status=?', $post['cabang'], '1', '1', "$s"));
            $c = count($users);

            if ($c != 0) {
                foreach ($users as $sky => $svl) {
                    $h = $obj_survei->save_survei($post, $svl->id);
                    $obj_survei->survei_reset();

                    $pesan = "Hai, " . $svl->name . " <br/><br/>";
                    $pesan .="<p>Anda mendapat request data survei dari:<br/>";
                    $pesan .="<strong>Calon Konsumen:</strong>";
                    $pesan .="<br/>Nama : " . $post['nama_client'];
                    $pesan .="<br/>Alamat : " . $post['alamat_client'];
                    $pesan .="<br/>KTP : " . $post['ktp_client'];
                    $pesan .="<br/>Telepon : " . $post['telephone_client'];
                    $pesan .="</p>";
                    $pesan .="<p>dengan Agen yang mereferensikan:<br/>";
                    $pesan .="Nama Agen: " . $obj_users->get_user('id', $post['ref_id'], 'name') . ' ' . $obj_users->get_user('id', $post['ref_id'], 'last_name');
                    $pesan .="<br/>NO VA : " . $obj_users->get_user('id', $post['ref_id'], 'ewallet_login');
                    $pesan .="</p>";

//                    $pesan .="<p>Anda mendapat request data survei dari <b>$post[nama_client]</b> yang beralamat di $post[alamat_client]</p>";
                    $pesan .="<p>silahkan check di <a href='" . $f3->get('ROOT') . "/?goto=" . $f3->get('ROOT') . "/message?ms=" . $h->id . "'>nabungtanpauang.com</a></p>";
                    $pesan .="\r\n powered by :nabungtanpauang.com";

                    $title = "Pemberitahuan Survei";
                    $subject = "Pemberitahuan Survei Adira " . $sub;
                    $user = array('name' => 'admin', 'email' => 'admin@nabungtanpauang.com');
                    $this->mail_broadcast($subject, $title, $pesan, $svl->mail, $user);
                }
            } else {
                $userss = $obj_users->get_users_by_role(array('cabang=? and active=? and is_mail_confirm=? and status=?', $post['cabang'], '1', '1', "3"));
//                $cabang_email = $obj_users->get_user('cabang', $post['cabang'], 'mail');
//                $cabang_f = $obj_users->get_user('cabang', $post['cabang'], 'name');
//                print_r($userss);
                if (!$userss) {
                    // echo "tidak ada user";
                    $user_all = $obj_users->get_users_by_role(array('cabang=? and active=? and is_mail_confirm=? ', $post['cabang'], '1', '1'));
                    $userss = $user_all;
                }
                // echo '<pre style="display:none">';
                // print_r($c);
                // print_r($post);
                // print_r($userss);
                // print_r($user_all);
                // echo '</pre>';
                // die('undermaintenenece');
                $cabang_email = $userss[0]->mail;
                $cabang_f = $userss[0]->name;
//                $userIdCabang = $obj_users->get_user('cabang', $post['cabang'], 'id');
                $userIdCabang = $userss[0]->id;
                /* insert ke survei */
                $s = $obj_survei->save_survei($post, $userIdCabang);
                $pesan = "Hai, $cabang_f <br/><br/>";
                //$pesan .="<p>Anda mendapat request data survei dari <b>$post[nama_client]</b> yang beralamat di $post[alamat_client]</p>";
                $pesan .="<p>Anda mendapat request data survei dari:<br/>";
                $pesan .="<strong>Calon Konsumen:</strong>";
                $pesan .="<br/>Nama : " . $post['nama_client'];
                $pesan .="<br/>Alamat : " . $post['alamat_client'];
                $pesan .="<br/>KTP : " . $post['ktp_client'];
                $pesan .="<br/>Telepon : " . $post['telephone_client'];
                $pesan .="</p>";
                $pesan .="<p>dengan Agen yang mereferensikan:<br/>";
                $pesan .="Nama Agen : " . $obj_users->get_user('id', $post['ref_id'], 'name') . ' ' . $obj_users->get_user('id', $post['ref_id'], 'last_name');
                $pesan .="<br/>NO VA : " . $obj_users->get_user('id', $post['ref_id'], 'ewallet_login');
                $pesan .="</p>";
                $pesan .="<p>silahkan check di <a href='" . $f3->get('ROOT') . "/?goto=" . $f3->get('ROOT') . "/message?ms=" . $s->id . "'>nabungtanpauang.com</a></p>";
                $pesan .="\r\n powered by :nabungtanpauang.com";
//            echo $pesan;
//            exit();
                $title = "Pemberitahuan Survei";
                $subject = "Pemberitahuan Survei Adira (Channeling & Retention)";
                $user = array('name' => 'admin', 'email' => 'admin@nabungtanpauang.com');
                $this->mail_broadcast($subject, $title, $pesan, $cabang_email, $user);
            }
        } else
            $f3->reroute('/');

        // echo '<pre style="display:none">';
        // print_r($c);
        // print_r($post);
        // print_r($users);
        // echo '</pre>';
        // die('undermaintenenece');
        $f3->reroute('/survei/terimakasih');
        // echo \Template::instance()->render('users/user_survei_message.htm');
    }

    function broadcast($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $session = $f3->get('SESSION');

        //check, jika tidak ada session user & role user != 0

        if (!isset($session['user']) || $session['user']['role'] != 0) {
            $f3->reroute('/');
        }
        //array( [user_id] => 3 [user_login] => user2 [role] => 0 [is_mail_confirm] => 1 [code] => 225be7 )
        $f3->set('user', $session['user']);
        $f3->set('obj_users', new \models\Users());
        $f3->set('obj_config', new \models\config());
        echo \Template::instance()->render('users/user_broadcast_message.htm');
    }

    function send_broadcast($f3) {
        $post = $f3->get('POST');
        $session = $f3->get('SESSION');
        $root = ($f3->get('ROOT'));
        $user = $session['user'];

        $obj_clikc = new \models\Click();
        $obj_user = new \models\Users();

        if (empty($post) && empty($session))
            $f3->reroute('/');
        else {
            $mail = $obj_user->get_user('id', $user['user_id'], 'mail');
//            echo $mail;

            $agen = array(
                'name' => ucfirst(base64_decode($post['tokenName'])),
                'user_id' => $user['user_id'],
                'email' => $mail
            );
            $namaagen = ucfirst($agen['name']);
            $subject = "Nabungtanpauang.com";
            $title = "Cara Mudah Menambah Tabungan Anda";
            $emails = explode(';', $post['toEmail']);
            for ($i = 0; $i < count($emails); $i++) {
                $data = array('user_id' => $user['user_id'], 'email_nasabah' => $emails[$i]);
                if ($obj_clikc->check_count($data) == 0) {
                    /*
                     * $this->user_id = $data['user_id'];
                      $this->email_nasabah = $data['email_nasabah'];
                     */
                    $s = ($obj_clikc->add_click($data));
                    $link_id = ($s->id);
                    $obj_clikc->reset();
                } else {
                    $d = $obj_clikc->get_data($data);
                    $link_id = ($d->id);
                }
                $link = "<a href='" . $root . "/redir?code=" . base64_encode($link_id) . "&nuid=" . $link_id . "'>link</a>";
//                if (!empty($post['mail_template']) && $post['mail_template'] == 1) {
                if (empty($post['mail_template'])) {
//                    $pesan = "Teman Anda, " . $namaagen . " <br/>" .
//                            "Mengajak Anda untuk memiliki produk  melalui ADIRA Finance.<br/>" .
//                            "Proses pengajuan pembiayaan yang mudah serta angsuran yang ringan menjadi jaminan kemudahan Anda <br/>" .
//                            "Klik $link  berikut untuk melanjutkan proses ini.";
                    $pesan = "<a href='" . $root . "/redir?code=" . base64_encode($link_id) . "&nuid=" . $link_id . "'><img src='" . $root . "/assets/img/email.jpg'/></a>";
                } else {
//                    $pesan = $post['mail_template'] . + "\r\nKlik <a href='$link'>link</a>  berikut untuk melanjutkan proses ini.";
                    $pesan1 = str_replace('{link}', $link, $post['toPesan']);
                    $pesan = str_replace('{nama}', $namaagen, $pesan1);
                    $pesan = nl2br($pesan);
                    if (strpos($post['toPesan'], '{link}') !== false) {
                        
                    } else {
                        $pesan .="<p>Klik $link  berikut untuk melanjutkan proses ini.</p>";
                    }
//$pesan .="<p>Klik $link  berikut untuk melanjutkan proses ini</p>"                    ;
                }
//                echo "<p>Untuk lebih jelas, silahkan klik link ini : <a href='$link'>link survei</a> </p><hr/>";
                //disni send email
                $this->mail_broadcast($subject, $title, $pesan, $emails[$i], $agen);
            }
//            
            $f3->reroute('/dashboard?b=s');
        }
    }

    function terimakasih($f3) {
        echo \Template::instance()->render('users/user_survei_message.htm');
    }

    function mail_broadcast($subject, $title, $pesan, $to, $from) {
        $message = "
            
            <html>
                <head>
                  <title>$title</title>
                </head>
                <body>
                  $pesan
                </body>
            </html>
                        
                    ";

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Additional headers

        $headers .= "To: $to " . "\r\n";
        if (is_array($from))
            $headers .= "From: " . $from['name'] . " <" . $from['email'] . ">" . "\r\n";
        else
            $headers .= "From: $from " . "\r\n";

        mail($to, $subject, $message, $headers);
    }

    function direct_input($f3) {
        $post = $f3->get('POST');
        $get = $f3->get('GET');
        $session = $f3->get('SESSION');

        if (empty($session['user']) || $session['user']['role'] != 0) {
            $f3->reroute('/');
        }

        $obj_clikc = new \models\Click();
        $obj_area = new \models\Area();


        $data = array(
            'agen' => $session['user']['user_id'],
            'link_id' => 0
        );
        $area = $obj_area->get_areas();

        $f3->set('areas', $area);
        $f3->set('user', $data);
        echo \Template::instance()->render('users/user_survei_form.htm');
    }

    function get_status($code, $i) {
        $s = array();
        switch ($code):
            case 1:
                $s['n'] = 'Proses';
                $s['c'] = 'label-default';
                $s['t'] = 'default';
                break;
            case 2:
                $s['n'] = 'Survey';
                $s['c'] = 'label-primary';
                $s['t'] = 'primary';
                break;
            case 3:
                $s['n'] = 'Di terima';
                $s['c'] = 'label-success';
                $s['t'] = 'success';
                break;
            case 4:
                $s['n'] = 'Di tolak';
                $s['c'] = 'label-danger';
                $s['t'] = 'danger';
                break;
        endswitch;
        return $s[$i];
    }

    function get_count_point($id) {
        $ob_survei = new \models\Survei();
        $result = $ob_survei->get_point_by_user($id);
        $j = 0;
        for ($i = 0; $i < count($result); $i++) {
            $j += $result[$i]->point;
        }
        echo $j;
    }

    function print_all($f3, $params) {
        $session = $f3->get('SESSION');
        $post = $f3->get('POST');
        $get = $f3->get('GET');
//        if (empty($session) || empty($post)) {
//            return false;
//        }
        if (empty($session)) {
            return $f3->reroute('/dashboard');
        }


        $all_id = explode(',', $params['id']);
//        $all_id = array($params['id']);
        if (count($all_id) > 0) {
            $obj_survei = new \models\Survei;
            $obj_category = new \models\Kategori();
            $obj_substation = new \models\Substation();
            $obj_user = new \models\Users();
            $obj_produk = new \models\Produk();
            $pdf = new \Fpdf\FPDF();

            for ($index = 0; $index < count($all_id); $index++) {
                $survei = $obj_survei->get_data($all_id[$index]);
                $id_produk = $survei->id_produk;
                $id_agen = $survei->id_agen;
                $id_user_cabang = $survei->id_user_cabang;
                $agen = $obj_user->get_user_by_id($id_agen);
                $user_cabang = $obj_user->get_user_by_id($id_user_cabang);
                $produk = null;
                $id_produk = $survei->id_produk;
                if (!empty($id_produk)) {
                    $produk = $obj_produk->get_produk($id_produk);
                }

                $pdf->AddPage();
                $pdf->SetFont('Arial', 'B', 26);
                $pdf->Cell(120, 10, 'Data Pengajuan Kredit ' . $agen->name . ' ' . $agen->last_name);
                $pdf->Ln(10);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(120, 10, 'ADIRA/' . $obj_substation->get_substation($survei->id_cabang, 'name') . '/' . date('dmy', strtotime($survei->date_save)) . '/' . $agen->ewallet_login . '/' . $survei->id);
                // Line break
                $pdf->Ln(10);
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Cell(25, 10, 'Data Agen');
                $pdf->Cell(5, 10, ':');
                $pdf->Ln(8);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(40, 10, 'Nama Agen');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $agen->name . ' ' . $agen->last_name);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Alamat Agen');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $agen->address);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'No Telepon Agen');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $agen->telephone);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Email Agen');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $agen->mail);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'No VA');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $agen->ewallet_login);
                $pdf->Ln(15);
                $pdf->Cell(190, 1, '', 1, '', '', 'yes');
                $pdf->Ln(8);

                /* data nasabag */
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Cell(25, 10, 'Data Nasabah :');
                $pdf->Ln(8);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(40, 10, 'Nama ');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $survei->nama);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'No KTP ');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $survei->ktp);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'No Telepon ');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $survei->telephone);
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Alamat ');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $survei->alamat);
                $pdf->Ln(8);
                $pdf->Ln(15);
                $pdf->Cell(190, 1, '', 1, '', '', 'yes');
                $pdf->Ln(8);
                /* data produk */
                if (!empty($produk)) {
                    $pdf->SetFont('Arial', 'B', 14);
                    $pdf->Cell(25, 10, 'Data Produk :');
                    $pdf->Ln(8);
                    $pdf->SetFont('Arial', '', 14);
                    $pdf->Cell(40, 10, 'Nama Produk');
                    $pdf->Cell(5, 10, ':');
                    $pdf->Cell(50, 10, $produk->produk . '(' . $obj_category->get_kategori_field($produk->merek, 'nama') . ')');
                    $pdf->Ln(8);
                    $pdf->Cell(40, 10, 'Jenis Produk');
                    $pdf->Cell(5, 10, ':');
                    $pdf->Cell(50, 10, $obj_category->get_jenis($survei->jenis_produk));
                } else {
                    $pdf->Cell(40, 10, 'Jenis Produk');
                    $pdf->Cell(5, 10, ':');
                    $pdf->Cell(50, 10, $obj_category->get_jenis($survei->jenis_produk));
                    $pdf->Ln(8);
                    $pdf->Cell(40, 10, 'Jumlah Maxi');
                    $pdf->Cell(5, 10, ':');
                    $pdf->Cell(50, 10, 'Rp' . number_format($survei->dana, 0, '', '.') . ',-');
                }
                $pdf->Ln(8);
                $pdf->Cell(190, 0.5, '', 1, '', '', 'yes');
                $pdf->Ln();
                $pdf->Cell(85, 10, 'Nama CRH yang bertanggung jawab');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $user_cabang->name . ' ' . $user_cabang->last_name);
                $pdf->Ln(8);
                $pdf->Cell(15, 10, 'Pesan');
                $pdf->Cell(5, 10, ':');
                $pdf->Ln(10);

                $pdf->MultiCell(190, 8, $survei->more_text);
                $pdf->Ln(5);
            }
            $pdf->Output('Adira_all_' . implode(',', $all_id) . '.pdf', 'D');
        } else {
            $f3->reroute('/');
        }
    }

    function admin_survei($f3) {
        $session = $f3->get('SESSION');
        $get = $f3->get('GET');

        if (empty($session) || $session['user']['role'] != 1) {
            $f3->reroute('/');
        }

        $m_survei = new \models\Survei();
        $m_cabang = new \models\Substation();
        $m_user = new \models\Users();
        $obj_category = new \models\Kategori();
        $f3->set('m_erase', false);



        if (!empty($get['hooh']) && $get['hooh'] == 'hapus') {
            if (!empty($get['mene']) && is_numeric($get['mene'])) {
                $m_survei->update_delete($get['mene'], 'D');
                $f3->set('m_erase', true);
            }
        }

        $group = null;
        $where = array('enval=?', 'A');
        $order_by = 'date_save';
        if (!empty($get['hoe'])) {
            $hoe = $get['hoe'];
            switch ($hoe) {
                case 'date':
                    $order_by = 'date_save DESC';
                    break;
                case 'terima':
                    $where = array('enval=? and status=?', 'A', 3);
//                    $group = 'status';
                    break;
                case 'tolak':
                    $where = array('enval=? and status=?', 'A', 4);
                    break;
                case 'survey':
                    $where = array('enval=? and status=?', 'A', 2);
                    break;
                case 'proses':
                    $where = array('enval=? and status=?', 'A', 1);
                    break;
            }
        }
        $limit = 20;
//        $count_all = $m_survei->get_count_data(array('enval=?', 'A'));
        $count_all = $m_survei->get_count_data($where);
        $pages = new \pagination($count_all, $limit);


        $order = array('order' => $order_by, 'limit' => $limit, 'offset' => $pages->getItemOffset(), 'group' => $group);
        $result = $m_survei->survei_all($where, $order);
        $showpaging = true;
        $file_template = "admin_survei_list.htm";
        if (!empty($get['hoe'])) {
            switch ($hoe) {
                case 'agen':
                    $dbh = $f3->get('DB');
                    $query = "SELECT orang.`name`, orang.id as id_orang, survei.* FROM users AS orang INNER JOIN survei ON orang.id = survei.id_agen ORDER BY orang.`name` ASC LIMIT $limit OFFSET " . $pages->getItemOffset();
                    $result = $dbh->exec($query);
                    $file_template = "admin_survei_list_agen.htm";
                    break;
                case 'pic':
                    $dbh = $f3->get('DB');
                    $query = "SELECT orang.`name`, orang.last_name, orang.id AS is_user, survei.* FROM users AS orang INNER JOIN survei ON orang.id = survei.id_user_cabang WHERE orang.role= 2 ORDER BY orang.`name` ASC LIMIT $limit OFFSET " . $pages->getItemOffset();
                    $result = $dbh->exec($query);
                    $file_template = "admin_survei_list_agen.htm";
                    break;
            }
        }
        //sort by 
        //if searh by name agen
        if (!empty($get['s'])) {
            $s = $get['s'];
            $f3->set('s', $s);
            $f3->set('status', 'A');
            $sd = $m_user->select("*", "role = 0 AND (lower(`name`) LIKE '%$s%' OR lower(last_name) LIKE '%$s%') AND active = '1' ");
            if (count($sd) > 0) {
                $arr = array();
                foreach ($sd as $Kes => $vf) {
                    $arr[] = $vf->id;
                }
                $listid = implode(',', $arr);
                $result = $m_survei->select_s('*', "id_agen IN($listid) and enval = 'A'");
                $showpaging = false;
            }
        }

        $f3->set('obj_category', $obj_category);
        $f3->set('showpaging', $showpaging);
        $f3->set('pagebrowser', $pages->serve());
        $f3->set('result', $result);
        $f3->set('offset', $pages->getItemOffset());
        $f3->set('m_cabang', $m_cabang);
        $f3->set('m_user', $m_user);
        $f3->set('c_survei', new \controllers\Survei());
        $f3->set('content', "admin/$file_template");
        echo \Template::instance()->render('admin_index.htm');
    }

    function admin_view($f3, $id = null) {
        $session = $f3->get('SESSION');
        $get = $f3->get('GET');

        if (empty($session) || $session['user']['role'] != 1) {
            $f3->reroute('/');
        }
        if (!is_numeric($id['id'])) {
            $f3->reroute('/');
        }

        $m_survei = new \models\Survei();
        $m_cabang = new \models\Substation();
        $m_user = new \models\Users();
        $m_produk = new \models\Produk();
        $obj_category = new \models\Kategori();

        $survei_data = $m_survei->get_data($id['id']);
        $agen = $m_user->get_user_by_id($survei_data->id_agen);
        $produk = $m_produk->get_produk($survei_data->id_produk);

        if (!empty($get['action']) && $get['action'] == 'print') {

            $id_produk = $survei_data->id_produk;
            $id_agen = $survei_data->id_agen;
            $id_user_cabang = $survei_data->id_user_cabang;

            $pdf = new \Fpdf\FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 26);
            $pdf->Cell(120, 10, 'Data Pengajuan Kredit');
            $pdf->Ln(10);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(120, 10, 'ADIRA\\' . $m_cabang->get_substation($survei_data->id_cabang, 'name') . '\\' . date('dmy', strtotime($survei_data->date_save)) . '\\' . $agen->ewallet_login . '\\' . $survei_data->id);
            // Line break
            $pdf->Ln(10);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Cell(25, 10, 'Data Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Ln(8);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(40, 10, 'Nama Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->name . ' ' . $agen->last_name);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Alamat Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->address);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No Telepon Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->telephone);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Email Agen');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->mail);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No VA');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $agen->ewallet_login);
            $pdf->Ln(15);
            $pdf->Cell(190, 1, '', 1, '', '', 'yes');
            $pdf->Ln(8);

            /* data nasabag */
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Cell(25, 10, 'Data Nasabah :');
            $pdf->Ln(8);
            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(40, 10, 'Nama ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei_data->nama);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No KTP ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei_data->ktp);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'No Telepon ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei_data->telephone);
            $pdf->Ln(8);
            $pdf->Cell(40, 10, 'Alamat ');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $survei_data->alamat);
            $pdf->Ln(8);
            $pdf->Ln(15);
            $pdf->Cell(190, 1, '', 1, '', '', 'yes');
            $pdf->Ln(8);
            /* data produk */
            if (!empty($produk)) {
                $pdf->SetFont('Arial', 'B', 14);
                $pdf->Cell(25, 10, 'Data Produk :');
                $pdf->Ln(8);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(40, 10, 'Nama Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $produk->produk . '(' . $obj_category->get_kategori_field($produk->merek, 'nama') . ')');
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Jenis Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $obj_category->get_jenis($survei_data->jenis_produk));
            } else {
                $pdf->Cell(40, 10, 'Jenis Produk');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, $obj_category->get_jenis($survei_data->jenis_produk));
                $pdf->Ln(8);
                $pdf->Cell(40, 10, 'Jumlah Maxi');
                $pdf->Cell(5, 10, ':');
                $pdf->Cell(50, 10, 'Rp' . number_format($survei_data->dana, 0, '', '.') . ',-');
            }
            $pdf->Ln(8);
            $pdf->Cell(190, 0.5, '', 1, '', '', 'yes');
            $pdf->Ln();
            $pdf->Cell(85, 10, 'Nama CRH yang bertanggung jawab');
            $pdf->Cell(5, 10, ':');
            $pdf->Cell(50, 10, $m_user->get_user('id', $id_user_cabang, 'name') . ' ' . $m_user->get_user('id', $id_user_cabang, 'last_name'));
            $pdf->Ln(8);
            $pdf->Cell(15, 10, 'Pesan');
            $pdf->Cell(5, 10, ':');
            $pdf->Ln(10);

            $pdf->MultiCell(190, 8, $survei_data->more_text);
            $pdf->Ln(5);

            $pdf->Output('ADIRA-' . $agen->ewallet_login . "-" . str_replace(" ", "-", $m_cabang->get_substation($survei_data->id_cabang, 'name')) . '-' . $survei_data->id . '.pdf', 'D');
        }
        $f3->set('obj_kategori', $obj_category);
        $f3->set('survei_data', $survei_data);
        $f3->set('m_cabang', $m_cabang);
        $f3->set('m_user', $m_user);
        $f3->set('m_produk', $m_produk);
        $f3->set('c_survei', new \controllers\Survei());
        $f3->set('agen', $agen);
        $f3->set('produk', $produk);
        $f3->set('content', 'admin/admin_survei_details.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function admin_user($f3) {
        $session = $f3->get('SESSION');
        if (empty($session) || $session['user']['role'] != 1) {
            $f3->reroute('/');
        }
        $m_survei = new \models\Survei();
        $m_user = new \models\Users();
        $count = $m_survei->survei_all(array('id_agen !=?', 0), array('group' => 'id_agen'));
        $count = (count($count));
//        exit();
        $limit = 20;
        $pages = new \pagination($count, $limit);
        $result = $m_survei->select_s('id_agen, COUNT(survei.id_agen) as total', array('id_agen!=?', 0), array('group' => 'id_agen', 'limit' => $limit, 'offset' => $pages->getItemOffset()));

        $f3->set('m_user', $m_user);
        $f3->set('no', 0);
        $f3->set('show_pagination', true);
        $f3->set('limit', $limit);
        $f3->set('pagebrowser', $pages->serve());
        $f3->set('offset', $pages->getItemOffset());
        $f3->set('result', $result);
        $f3->set('content', 'admin/admin_survei_user.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function export($f3) {
        $session = $f3->get('SESSION');
        if (empty($session) || $session['user']['role'] != 1) {
            $f3->reroute('/');
        }
        $m_survei = new \models\Survei();
        $m_user = new \models\Users();
        $limit = 500;
        $count = $m_survei->get_count_data();
        $offset = ceil($count / $limit);

        $dataArray = array();
        $dataArray[] = array(
            '1' => 'No',
            '2' => 'No Va Agen',
            '3' => 'Nama Agen',
            '4' => 'Nama Nasabah',
            '5' => 'Tanggal masuk'
        );

        $date = date("j_M_Y");
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=export_survei_$date.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $outstream = fopen("php://output", "w");
        fputcsv($outstream, array(
            '1' => 'No',
            '2' => 'No Va Agen',
            '3' => 'Nama Agen',
            '4' => 'Nama Nasabah',
            '5' => 'Tanggal masuk'
                )
        );
//        for ($i = 0; $i < $offset; $i++) {
//            $plus = $i * $limit;
//            $result = $m_survei->survei_all('', array('limit' => $limit, 'offset' => $i + $plus));
//            foreach ($result as $key => $value) {
//                if (preg_match('/9689/i', $m_user->get_user('id', $value->id_agen, 'ewallet_login'))) {
////                    fputcsv($outstream, array(
////                        '1' => $key + 1,
////                        '2' => $m_user->get_user('id', $value->id_agen, 'ewallet_login'),
////                        '3' => $m_user->get_user('id', $value->id_agen, 'name') . " " . $m_user->get_user('id', $value->id_agen, 'last_name'),
////                        '4' => $value->nama,
////                        '5' => $value->date_save
////                    ));
//                    $dataArray[] = array(
//                        '1' => $key + 1,
//                        '2' => $m_user->get_user('id', $value->id_agen, 'ewallet_login'),
//                        '3' => $m_user->get_user('id', $value->id_agen, 'name') . " " . $m_user->get_user('id', $value->id_agen, 'last_name'),
//                        '4' => $value->nama,
//                        '5' => $value->date_save
//                    );
//                }
//            }
//        }

        for ($i = 0; $i < $offset; $i++) {
            $plus = $i * $limit;
            if ($plus)
                $option = array('limit' => $limit, 'offset' => $i + $plus);
            else {
                $option = array('limit' => $limit);
            }
            $result = $m_survei->survei_all(null, $option);
            foreach ($result as $key => $value) {
                $idagen = $m_user->get_user('id', $value->id_agen, 'ewallet_login');
                if (preg_match('/9689/i', $m_user->get_user('id', $value->id_agen, 'ewallet_login'))) {
                    $data = array(
                        '1' => $key + 1,
                        '2' => $idagen,
                        '3' => $m_user->get_user('id', $value->id_agen, 'name') . " " . $m_user->get_user('id', $value->id_agen, 'last_name'),
                        '4' => $value->nama,
                        '5' => $value->date_save
                    );
                    fputcsv($outstream, $data);
                }
            }
        }

//        foreach ($dataArray as $result) {
//            fputcsv($outstream, $result);
//        }
        fclose($outstream);

//        echo '<pre>';
//        print_r($dataArray);
//        echo '</pre>';
    }

}
