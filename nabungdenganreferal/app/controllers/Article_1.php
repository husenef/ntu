<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Article
 *
 * @author husen
 */

namespace controllers;

class Article {

    function index($f3) {
        $session = $f3->get('SESSION.user');
        if (empty($session) || $session['role'] != 1)
            $f3->reroute('/');

        $post = $f3->get('POST');
        $get = $f3->get('GET');
        $files = $f3->get('FILES');
        $obj_article = new \models\Article();


        if ($post) {
            $data = array();
            $data = array(
                'judul' => $post['judul'],
                'content' => $post['content'],
                'author' => $session['user_id'],
                'slug' => $post['slug'],
                'lokasi' => $post['lokasi']
            );
            if (!empty($post['featured']) && $post['featured'] == '4') {
                $idF = $obj_article->get_article_custom('is_featured', 1, 'id');
                if ($idF) {
                    $obj_article->update_f($idF);
                }
                $data['is_featured'] = '1';
                $data['link'] = 4;
            }

//            $data = array();


            if ($files) {
                $destination = './content/article/';
                $filename = md5(date('H:i:s d-m-y')) . '.jpg';
                if (move_uploaded_file($files['foto']['tmp_name'], $destination . $filename))
                    $data['foto'] = $filename;
            }
            if (!empty($post['link'])) {
                $data['link'] = $post['link'];
            }
//            print_r($data);
//            exit();
            //add
            $obj_article->add_article($data);
            $f3->reroute('/admin/article?notification=notification=cat.add');
        }

        if (!empty($get['remove'])) {
            $ar = $obj_article->get_article_by_id($get['remove']);
            if (!empty($ar)) {
                $destination = './content/article/';
                if (file_exists($destination . $ar->image) && !empty($ar->image)) {
                    unlink($destination . $ar->image);
                }
                $obj_article->del_article($ar->id);
            }
        }
         $f3->set('obj_article', new \models\Article());
        $f3->set('content', 'admin/admin_article.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function edit($f3, $params) {
        $post = $f3->get('POST');
        $id = $params['id'];
        $files = $f3->get('FILES');
        if (!is_numeric($id) || empty($id))
            $f3->reroute('/');
        $obj_article = new \models\Article();
        $data = array();
        if ($post) {


            $data = array(
                'judul' => $post['judul'],
                'content' => $post['content'],
                'slug' => strtolower($post['slug']),
                'link' => $post['link'],
                'lokasi' => $post['lokasi']
            );
            if (!empty($post['featured']) && $post['featured'] == '4') {
                $idF = $obj_article->get_article_custom('is_featured', 1, 'id');
                if ($idF) {
                    $obj_article->update_f($idF);
                }
                $data['is_featured'] = '1';
                $data['link'] = 4;
            }

            if ($files) {
                $destination = './content/article/';
                if (!empty($post['imageold'])) {
                    if (file_exists($destination . $post['imageold'])) {
                        unlink($destination . $post['imageold']);
                    }
                }

                $newfilename = md5(date('H:i:s d-m-y')) . '.jpg';
                @move_uploaded_file($files['foto']['tmp_name'], $destination . $newfilename);
                $data['foto'] = $newfilename;
            }
//            print_r($post);
//            exit();
            $obj_article->update_article($post['id'], $data);
            $f3->reroute('/admin/article?notification=succes.update');
        }

        $article = $obj_article->get_article_by_id($id);
        $alink = $obj_article->get_article_by_link_id($article->link);

        $f3->set('old_id', ($alink->id));
        $f3->set('article', $article);
        $f3->set('content', 'admin/admin_article_edit.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function views($f3, $params) {
//        $id = $this->check_url($params['link']);

        if (empty($params))
            $f3->reroute('/');

        $obj_article = new \models\Article();
//        $article = $obj_article->get_article_by_link_id($id);
        $article = $obj_article->get_article_custom('slug', $params['link']);
        if (empty($article)) {
            $article = '404';
        }
        $f3->set('article', $article);
        $f3->set('content', '/users/article_view.htm');
        echo \Template::instance()->render('user_index.htm');
    }

//    function check_url($url) {
//
//        switch ($url) {
//            case 'kebijakan-aturan':
//                $id = '2';
//                break;
//            case 'faq':
//                $id = '3';
//                break;
//            case 'ketentuan-aturan':
//                $id = '4';
//                break;
//            case 'hubungi-kami':
//                $id = '5';
//                break;
//            default:
//                $id = '0';
//                break;
//        }
//        return $id;
//    }
}
