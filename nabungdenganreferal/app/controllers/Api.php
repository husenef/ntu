<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author Husen
 */

namespace controllers;

class Api {

    //put your code here
    function index($f3) {
        /*
          yang dikirim
         * $post = array(
         * 'apicode'=>'generate string',
         * 'vaCode'=>'va-code')
         */
        $post = $f3->get('POST');
        $myCode = '4sxohwuzm791prsb2';
        $clientCode = $post['apicode'];
        if ($myCode == $clientCode) {
            if (!empty($post['vaCode'])) {
                return 0;
            } else {
                $obj_user = new \models\Users();
                $return = $obj_user->get_user_by_id($post['vaCode']);
                if (is_object($return)) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else {
            return 0;
        }
    }

    function check($f3) {
        $post = $f3->get('POST');
        if (!$post)
            return false;
//        print_r($post['obligorid']);
        $url = 'http://www.adirabox.com/api/cekobligor.php';

// Prepare Parameters
        $params = array(
            'obligorid' => $post['obligorid'], // API Key Merchant / Penjual
        );

        $params_string = http_build_query($params);

//open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

//execute post
        $request = curl_exec($ch);

        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
//            $result = json_decode($request, true);
//            echo json_encode($result);
            echo $request;
        }

//close connection
        curl_close($ch);
    }

    function komisi($f3) {
        $post = $f3->get('POST');
        if (!$post)
            return false;

        // URL Payment IPAYMU
        $url = 'http://www.adirabox.com/api/getkomisi.php';

// Prepare Parameters
//        $params = array(
//            'username' => 'margita.mail@gmail.com', // API Key Merchant / Penjual
//            'password' => 'rahasia123',
//        );
        $params = array(
            'username' => $post['obligorid'], // API Key Merchant / Penjual
            'password' => $post['password'],
        );

        $params_string = http_build_query($params);

//open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

//execute post
        $request = curl_exec($ch);

        if ($request === false) {
            echo 'Curl Error: ' . curl_error($ch);
        } else {
//            $result = json_decode($request, true);
            print_r($request);
        }

//close connection
        curl_close($ch);
    }

}
