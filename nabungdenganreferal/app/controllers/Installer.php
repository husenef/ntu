<?php

namespace controllers;

class Installer {

    function install($f3) {
        //die(print_r($f3->get('SERVER')));
        $root = $f3->get('SERVER[REQUEST_URI]');
        $root = 'http://' . $f3->get('SERVER[HTTP_HOST]') . str_replace('/install', '', $root);
        $f3->set('root', $root);


        if ($f3->get('POST')) {
            $siteroot = $f3->get('POST.root');
            $mysql_host = $f3->get('POST.mysql_host');
            $mysql_port = $f3->get('POST.mysql_port');
            $mysql_db = $f3->get('POST.mysql_db');
            $mysql_user = $f3->get('POST.mysql_user');
            $mysql_pass = $f3->get('POST.mysql_pass');
            $lang = $f3->get('POST.language');


            $link = mysql_connect($root = $mysql_host . ':' . $mysql_port, $mysql_user, $mysql_pass);
            if ($link) {
                $data = "
INSTALLED       = TRUE
UI              = app/templates/
TEMP            = app/data/tmp/
CACHE           = FALSE
AUTOLOAD        = app/
PAGINATION      = 20
LOCALES         = app/dict/
LANGUAGE        = $lang
LOGS            = app/data/
ROOT            = \"$siteroot\"

mysql_host      = \"$mysql_host\"
mysql_port      = \"$mysql_port\"
mysql_db        = \"$mysql_db\"
mysql_user      = \"$mysql_user\"
mysql_pass      = \"$mysql_pass\"
                    ";

                file_put_contents('app/config.ini', $data);

                mysql_query("DROP DATABASE `$mysql_db`", $link);
                mysql_query("CREATE DATABASE IF NOT EXISTS `$mysql_db`", $link);

                mysql_select_db($mysql_db, $link);

                $brokers = file_get_contents('app/schema/brokers.sql');
                mysql_query($brokers, $link);

                $ewallets = file_get_contents('app/schema/ewallets.sql');
                mysql_query($ewallets, $link);

                $history = file_get_contents('app/schema/history.sql');
                mysql_query($history, $link);

                $users = file_get_contents('app/schema/users.sql');
                mysql_query($users, $link);

                $q = "INSERT INTO `users` (`id`, `code`, `role`, `name`, `last_name`, `mail`, `country`, `ewallet_id`, `ewallet_login`, `user_login`, `user_password`, `is_mail_confirm`) VALUES
                (1, 'a0d1m3', 1, 'none', 'none', 'none', 'none', NULL, NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1)";
                mysql_query($q, $link);

                $config = file_get_contents('app/schema/config.sql');
                mysql_query($config, $link);

                $config_values = file_get_contents('app/schema/config_data.sql');
                mysql_query($config_values, $link);

                $status = file_get_contents('app/schema/status.sql');
                mysql_query($status, $link);

                $status_values = file_get_contents('app/schema/status_data.sql');
                mysql_query($status_values, $link);



                $f3->reroute('/');
            }
        }

        echo \Template::instance()->render('page_install.htm');
    }

}
