<?php

namespace controllers;

class Security {

    private $m;
    private $obligorid;

    function __construct() {
        $this->f3 = \Base::instance();
        $this->m = new \DB\SQL\Mapper($this->f3->get('DB'), 'users');
    }

    function login($f3) {
        $post = $f3->get('POST');
        $f3->scrub($post);
        $session = $f3->get('SESSION');


        //seprtinya di sini loginnya
        if ($u = $this->bind($post['username'], md5($post['password']))) {

            /*
             * save log users
             */
            $logs = new \models\Log();
            $data = $this->user_log($u['user_id']);
            $logs->add_logs($data);


            $f3->set('SESSION.user', $u);
            $f3->set('COOKIE.user.user_id', $u['user_id'], 3600);
            if (!empty($session['goto'])) {
                $f3->reroute($session['goto']);
                $f3->clear('SESSION.goto');
            } else {
                $f3->reroute('/dashboard');
            }
        } else {
            $f3->set('sis_error', 'error.credentials');
            $f3->reroute('/?notification=error.credentials');
        }
    }

    function login_view($f3) {
        $session = $f3->get('SESSION');
        $get = $f3->get('GET');
        if (!empty($get['goto'])) {
            $f3->set('SESSION.goto', $get['goto']);
        }
        if ($this->check_login()) {
            if (!empty($session['goto'])) {
                $target = $session['goto'];
                $f3->clear('SESSION.goto');
                $f3->reroute($target);
            } else {
                $f3->reroute('/dashboard');
            }
        };
        $f3->set('obj_article', new \models\Article());
//user_index
        $f3->set('content', 'users/page_login.htm');
        echo \Template::instance()->render('user_index.htm');
    }

    function logout($f3) {
        $f3->clear('SESSION');
        $f3->reroute('/');
    }

    function check_login() {
        return (($this->f3->get('SESSION.user')) && ($this->f3->get('SESSION.user.is_mail_confirm'))) ? TRUE : FALSE;
    }

    function bind($username, $password) {
        $backends = array('__sql');
        for ($i = 0; $i < count($backends); $i++) {
            $r = $this->$backends[$i]($username, $password);
            if ($r) {
                return $r;
            }
        }

        return FALSE;
    }

    function __sql($username, $password) {
        $var = $this->m->findone(array('user_login=? AND user_password=? AND is_mail_confirm=? AND active=?', $username, $password, '1', '1'));

        if ($var) {
            return array('user_id' => $var->id, 'user_login' => $var->user_login, 'role' => $var->role, 'is_mail_confirm' => $var->is_mail_confirm, 'code' => $var->code);
        } else {
            return null;
        }
    }

    function check($f3) {

        if (empty($this->obligorid))
            return false;
        $url = 'http://www.adirabox.com/api/cekobligor.php';

// Prepare Parameters
        $params = array(
            'obligorid' => $this->obligorid, // API Key Merchant / Penjual
        );

        $params_string = http_build_query($params);

//open connection
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

//execute post
        $request = curl_exec($ch);

        if ($request === false) {
            return 'Curl Error: ' . curl_error($ch);
        } else {
//            $result = json_decode($request, true);
//            echo json_encode($request);
            return $request;
        }

//close connection
        curl_close($ch);
    }

    function user_log($id) {
        date_default_timezone_set('Asia/Jakarta');
        //ASSIGN VARIABLES TO USER INFO
        $time = date("Y-m-d H:i:s");
        $ip = getenv('REMOTE_ADDR');
        $userAgent = getenv('HTTP_USER_AGENT');
        $referrer = getenv('HTTP_REFERER');

//COMBINE VARS INTO OUR LOG ENTRY
//        $msg = "IP: " . $ip . " TIME: " . $time . " REFERRER: " . $referrer . " SEARCHSTRING: " . $query . " USERAGENT: " . $userAgent;
        $msg = array(
            'time' => $time,
            'ip' => $ip,
            'browser' => $userAgent,
            'referer' => $referrer,
            'id_user' => $id
        );
//CALL OUR LOG FUNCTION
        return $msg;
    }

}
