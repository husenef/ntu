<?php

namespace controllers;

class Tools {

    function get_merek($f3) {
        $post = $f3->get('POST');
//        $get = $f3->get('GET');
        if (empty($post))
            return FALSE;

        $obj_produk = new \models\Produk();
        $obj_kategori = new \models\Kategori();

        $data = $obj_produk->get_merek($post['jenis']);
        $html = "<div class=\"form-group\">";
        $html .="<label>Merek</label>";
        $html .= "<select id=\"merek\" name=\"merek\" required=\"required\" onchange='update_type()' class='form-control'>";
        $html .="<option value=''> -- pilih merek -- </option>";
        foreach ($data as $key => $val) {
            $html.="<option value='" . $obj_kategori->get_kategori_field($val->merek, 'id_cat') . "' data-id=\"$post[jenis]\">" . $obj_kategori->get_kategori_field($val->merek, 'nama') . "</option>";
        }
        $html .="<option value='others' >LAINNYA</option>";
        $html .= '</select>';
//        $merek = $obj_kategori->get_cat();
        echo $html;
    }

    function get_produk($f3) {
        $post = $f3->get('POST');
        if (!$post) {
            return null;
        }
        $obj_produk = new \models\Produk();
        $produk = $obj_produk->get_produk_by_jenis_merek($post['jenis_id'], $post['merek_id']);
        $html = "<div class=\"form-group\">";
        $html .="<label>Produk</label>";
        $html .= "<select id=\"Produk\" name=\"Produk\" required=\"required\" onchange='cek_produk(this)' class='form-control'>";
        $html .="<option value=''> -- pilih Produk -- </option>";
        foreach ($produk as $key => $val) {
            $html.="<option value='" . $val->id . "' >" . $val->produk . "</option>";
        }
        $html .="<option value='lainnya'>LAINNYA</option>";
        $html .= '</select>';
//        $merek = $obj_kategori->get_cat();
        echo $html;
    }

    function get_cabang($f3) {
        $post = $f3->get('POST');
        if (!$post) {
            return null;
        }

        $Obj_cabang = new \models\Substation();
        $cabang = $Obj_cabang->get_substation_by_area($post['aread_id']);
        if (count($cabang) == 0)
            return FALSE;
        else {
            $html = "<div class=\"form-group\">";
            $html .="<label>Cabang</label>";
            $html .= "<select id=\"cabang\" name=\"cabang\" required=\"required\" onchange='' class='form-control'>";
            $html .="<option value=''> -- pilih cabang -- </option>";
            foreach ($cabang as $key => $val) {
                $html.="<option value='" . $val->id . "' >" . $val->name . "</option>";
            }
            $html .= '</select>';
//        $merek = $obj_kategori->get_cat();
            echo $html;
        }
    }

    function get_jenis_produk($code) {
        $codes = explode(',', $code);
//        print_r($codes);
        $h = array();
        for ($i = 0; $i < count($codes); $i++) {
            switch ($codes[$i]) {
                case 1:
                    $h[] = 'Mobil Baru';
                    break;
                case 2:
                    $h[] = 'Mobil Bekas';
                    break;
                case 3:
                    $h[] = 'Mobil Maxi';
                    break;
                case 4:
                    $h[] = 'Motor Baru';
                    break;
                case 5:
                    $h[] = 'Motor Bekas';
                    break;
                case 6:
                    $h[] = 'Motor Maxi';
                    break;

                default:
                    $h[] = "";
                    break;
            }
        }
        echo implode(', ', $h);
    }

    function get_user($f3) {
        $post = $f3->get('POST');
        if (empty($post))
            return false;
        $obj_user = new \models\Users();
        $users = $obj_user->get_users_by_role(array('cabang=? AND active=?', $post['cabang_id'], '1'));
        if (count($users) > 1) {
            $html = "<div class=\"form-group\">";
            $html .="<label>Tujuan</label>";
            $html .= "<select id=\"cabang\" name=\"user_cabang\" required=\"required\" onchange='' class='form-control'>";
            $html .="<option value=''> -- pilih user -- </option>";
            foreach ($users as $key => $val) {
                $html.="<option value='" . $val->id . "' >" . $val->name . "</option>";
            }
            $html .= '</select>';
            echo $html;
        } else {
            return null;
        }
    }

    function export_user($f3, $params) {
        $session = $f3->get('SESSION');

        if (empty($params['code']) || empty($session['user']) || $session['user']['role'] != '1')
            $f3->reroute('/');

        switch ($params['code']) {
            case 'agen':
                $role = 0;
                $url = 'agen';
                break;

            case'cabang':
                $role = 2;
                $url = 'cabang';
                break;
        }
        $limit = 1000;

        $obj_user = new \models\Users();
        $total = $obj_user->get_user_by_query_count("role = $role and active='1'");

        $offset = ceil($total / $limit);


        $arrayUser = "";
        $arrayUserHead = array(
            '1' => 'NO',
            '2' => 'No Va',
            '3' => "Nama $url",
            '4' => "Tanggal Masuk",
            "5" => "Email",
            "6" => "Alamat",
        );


        $date = date("j_M_Y");
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=data_export_user_$url" . "_$date.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $fv = fopen("php://output", "w");

//        foreach ($arrayUser as $value) {
//            fputcsv($fv, $value);
//        }
        fputcsv($fv, $arrayUserHead);
        for ($i = 0; $i < $offset; $i++) {
            $plus = $i * $limit;
            $no = 0;
            $user_list = $obj_user->get_users_by_role(array('role=? and active=? ', $role, '1'), array('limit' => $limit, 'offset' => $i + $plus));
            foreach ($user_list as $userKey => $user) {
                if (preg_match('/9689/i', $user->ewallet_login)) {
                    $arrayUser = array(
//                        '1' => $userKey + 1,
                        '1' => $no + 1 + $plus,
                        '2' => "'" . $user->ewallet_login,
                        '3' => $user->name . " " . $user->last_name,
                        '4' => $user->date,
                        "5" => $user->mail,
                        "6" => $user->address,
                    );
                    fputcsv($fv, $arrayUser);
                    $no++;
                }
            }
        }
        fclose($fv);
    }

}
