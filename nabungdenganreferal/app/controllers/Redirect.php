<?php

namespace controllers;

class Redirect {

    function redirect($f3) {
        $get = $f3->get('GET');

        $obj_clikc = new \models\Click();
        if (!isset($get['code']) && empty($get['code'])) {
            $f3->reroute('/');
        }
        $data = array(
            'link_id' => base64_decode($get['code']),
            'date_click' => date('Y-m-d H:i:s')
        );

        $linkdata = $obj_clikc->cari($data['link_id']);

        $obj_clikc->update_count($data['link_id'], $linkdata['clicked'] + 1);
        $f3->set('SESSION.ref', array(
            'link_id' => $data['link_id']
        ));
        $f3->reroute('/survei');
    }

}
