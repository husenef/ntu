<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produk
 *
 * @author Agus
 */

namespace controllers;

class Produk {

    function dashboard($f3) {
        $get = $f3->get('GET');
        $get = $f3->get('GET');
        $post = $f3->get('POST');
        $files = $f3->get('FILES');

        //$f3->scrub($post);
        $f3->scrub($get);

        $obj_produk = new \models\Produk();
        $obj_click = new \models\Click();
        $obj_kategori = new \models\Kategori();
        $obj_survei = new \models\Survei();

        if (isset($post['produk'])) {

            $imgArray = array('image/jpeg', 'image/png', 'image/gif');
            $post['foto'] = '';
            if (isset($files) && (in_array($files['produk_file']['type'], $imgArray))) {
                $filename = md5(date('H:i:s d-m-y')) . '.jpg';
                move_uploaded_file($files['produk_file']['tmp_name'], './assets/images/' . $filename);
                $post['foto'] = $filename;
            }
            $obj_produk->add_produk($post);
        }

        if (isset($get['action']) && $get['action'] == 'delete') {
//            $i = $obj_produk->get_produk($get['item'], 'foto');
//            $filename = getcwd() . '/assets/images/' . $i;
//            if (file_exists($filename))
//                unlink($filename);
            $obj_produk->del_produk($get['item']);
        }
        $all = $obj_produk->get_produks(array('active=?', '1'));


        $limit = 10;
//        $f3->set('limit', $limit);
        $pages = new \pagination(count($all), $limit);
        $f3->set('pagebrowser', $pages->serve());
//        $f3->set('offset', $pages->getItemOffset());

        $allproduk = $obj_produk->get_produks(array('active=?', '1'), array('order' => 'id DESC', 'limit' => $limit, 'offset' => $pages->getItemOffset()));
        $dklik = array();
        foreach ($allproduk as $kp => $kv) {
            $click = $obj_survei->get_survei('id_produk', $kv->id, array('group' => 'id_produk'));
            if ($click) {
                $dklik[$kp]['accepted'] = count($click);
                foreach ($click as $kc => $vc) {
                    $dklik[$kp]['diklik'] = $obj_click->get_data_custom($vc->id_link, 'clicked');
                }
            } else {
                $dklik[$kp]['accepted'] = 0;
                $dklik[$kp]['diklik'] = 0;
            }
        }
        //kategory parent
        $categories = $obj_kategori->get_kategori();
//        

        $f3->set('categories', $categories);
        $f3->set('obj_kategori', $obj_kategori);
        $f3->set('dklik', $dklik);
        $f3->set('allproduk', $allproduk);
        $f3->set('c_tools', new \controllers\Tools());
//        $f3->set('content', 'page_admin_produk.htm');
        $f3->set('content', 'admin/admin_produk.htm');
        $f3->set('obj_kategori', new \models\Kategori());
//        echo \Template::instance()->render('__layout.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function produk_categori($f3) {
        $get = $f3->get('GET');
        $post = $f3->get('POST');

        $obj_kategori = new \models\Kategori();

        //if update
        if ($post) {
//            print_r($post);
//            exit();
            if (!empty($post['id'])) {
                $obj_kategori->upd_kategori($post['id'], $post);
                $f3->reroute('/admin/produk_katergori?notification=cat.add');
            } else if (empty($post['id'])) {
                $obj_kategori->add_kategori($post);
                $f3->reroute('/admin/produk_katergori?notification=cat.update');
            } else {
                
            }
        }
        if (!empty($get['idcat']) && is_numeric($get['idcat'])) {
            if ($obj_kategori->get_child_count($get['idcat'])) {
                $child = $obj_kategori->get_child($get['idcat']);
                foreach ($child as $skey => $vasd) {
                    $obj_kategori->upd_kategori($vasd['id_cat'], array('nama_kategori' => $vasd['nama'], 'parent_id' => 0));
                }
            }
            $obj_kategori->del_cat($get['idcat']);
        }
        $categories = $obj_kategori->get_kategori();

        $f3->set('categories', $categories);
//        $f3->set('content', 'admin_produk_category.htm');
//        echo \Template::instance()->render('__layout.htm');
        $f3->set('obj_kategori', new \models\Kategori());
        $f3->set('content', 'admin/admin_produk_category.htm');
        echo \Template::instance()->render('admin_index.htm');
    }

    function get_cat($f3) {
        $post = $f3->get('POST');
        if (!is_numeric($post['id_parent']))
            echo '1';

        $id = $post['id_parent'];
        $obj_kategori = new \models\Kategori();
        if ($obj_kategori->get_child_count($id)) {
            $cat_by = $obj_kategori->get_child($id);
            $s = "<label class='control-label '>Merek</label>";
            $s .= "<select id='sub-" . $id . "' class='form-control' name='merek'>";
            foreach ($cat_by as $cbey => $cblue) {
                $s.= "<option value='" . $cblue['id_cat'] . "'>" . $cblue['nama'] . "</option>";
            }
            $s.="</select>";

            echo ($s);
        } else {
            echo '0';
        }
    }

}
