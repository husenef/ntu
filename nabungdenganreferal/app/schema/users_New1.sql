-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.8-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4482
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para refmanager_db
CREATE DATABASE IF NOT EXISTS `refmanager_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `refmanager_db`;


-- Volcando estructura para tabla refmanager_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` tinytext COLLATE utf8_bin,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `name` tinytext COLLATE utf8_bin,
  `last_name` tinytext COLLATE utf8_bin,
  `mail` text COLLATE utf8_bin,
  `country` text COLLATE utf8_bin,
  `ewallet_id` int(11) DEFAULT NULL,
  `ewallet_login` text COLLATE utf8_bin,
  `user_login` tinytext COLLATE utf8_bin NOT NULL,
  `user_password` text COLLATE utf8_bin NOT NULL,
  `is_mail_confirm` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla refmanager_db.users: ~9 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `code`, `role`, `name`, `last_name`, `mail`, `country`, `ewallet_id`, `ewallet_login`, `user_login`, `user_password`, `is_mail_confirm`) VALUES
	(1, 'a0d1m3', 1, 'none', 'none', 'none', 'none', NULL, NULL, 'admin', '202cb962ac59075b964b07152d234b70', 1),
	(2, 'd0c2d8', 0, 'John', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'jdoe', '21232f297a57a5a743894a0e4a801fc3', 0),
	(3, 'd0c2d9', 0, 'Pedro', 'Jose', 'pedro@gmail.com', 'España', 7, 'pedro@gmail.com', 'js', '21232f297a57a5a743894a0e4a801fc3', 0),
	(4, 'd0c3d8', 0, 'Raul', 'Doe', 'john@gmail.com', 'Cuba', 7, 'john@gmail.com', 'ra', '21232f297a57a5a743894a0e4a801fc3', 0),
	(5, 'd0c3d9', 0, 'Alfredo', 'Doe', 'al@gmail.com', 'Brasil', 7, 'john@gmail.com', 'al', '21232f297a57a5a743894a0e4a801fc3', 1),
	(6, 'd0v2d8', 0, 'Alberto', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'alb', '21232f297a57a5a743894a0e4a801fc3', 0),
	(7, 'd0q2d8', 0, 'Leonardo', 'Doe', 'john@gmail.com', 'Argentina', 7, 'john@gmail.com', 'leo', '21232f297a57a5a743894a0e4a801fc3', 0),
	(8, 'd0r2d8', 0, 'Ricardo', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'ric', '21232f297a57a5a743894a0e4a801fc3', 1),
	(9, 'd0a2d8', 0, 'John', 'Doe', 'john@gmail.com', 'Cuba', 10, 'john@gmail.com', 'j', '21232f297a57a5a743894a0e4a801fc3', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
