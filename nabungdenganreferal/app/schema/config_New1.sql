-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.8-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4482
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para refmanager_db
CREATE DATABASE IF NOT EXISTS `refmanager_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `refmanager_db`;


-- Volcando estructura para tabla refmanager_db.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` tinytext COLLATE utf8_bin,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla refmanager_db.config: ~0 rows (aproximadamente)
DELETE FROM `config`;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `value`, `data`) VALUES
	(1, 'title', 'Nombre'),
	(2, 'title_em', 'Sitio'),
	(3, 'subtitle', ''),
	(4, 'confirm_mail_title', 'Titulo del mail'),
	(5, 'confirm_mail_from', 'Nombresitio'),
	(6, 'confirm_mail_from_src', 'sitio@dominio.com'),
	(7, 'confirm_mail_subject', 'Asunto del correo'),
	(8, 'confirm_mail_message', 'Este es el mensaje'),
	(9, 'admin_mail_dir', ''),
	(10, 'admin_send_mail', 'false'),
	(11, 'page_register_text', ''),
	(12, 'page_user_text', NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
