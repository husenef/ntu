CREATE TABLE IF NOT EXISTS `brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` tinytext COLLATE utf8_bin,
  `desc` text COLLATE utf8_bin,
  `ref_link` text COLLATE utf8_bin,
  `reg_val` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

