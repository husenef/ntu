INSERT INTO `status` (`id`, `lang`, `desc`, `class`) VALUES
	(1, 'es', 'Registrado', 'text-danger'),
	(2, 'es', 'Depositado', 'text-warning'),
	(3, 'es', 'Gestionando Cobro', 'text-warning'),
	(4, 'es', 'Pagado', 'text-success'),
        (5, 'en', 'Registered', 'text-danger'),
        (6, 'en', 'Deposited', 'text-warning'),
        (7, 'en', 'Collection Management', 'text-warning'),
        (8, 'en', 'Paid', 'text-success');