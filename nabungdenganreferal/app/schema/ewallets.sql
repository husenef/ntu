CREATE TABLE IF NOT EXISTS `ewallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ewallet_type` tinytext COLLATE utf8_bin,
  `ewallet_url` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

