INSERT INTO `config` (`id`, `value`, `data`) VALUES
	(1, 'title', 'Referral'),
	(2, 'title_em', 'Manager'),
	(3, 'subtitle', 'earn much more with us!'),
	(4, 'confirm_mail_title', 'Mail title'),
	(5, 'confirm_mail_from', 'From'),
	(6, 'confirm_mail_from_src', 'site@domain.com'),
	(7, 'confirm_mail_subject', 'Subject'),
	(8, 'confirm_mail_message', 'Message'),
	(9, 'admin_mail_dir', ''),
	(10, 'admin_send_mail', 'false'),
	(11, 'page_register_text', ''),
	(12, 'page_user_text', NULL),
        (13, 'notify_mail_title', 'Mail title'),
        (14, 'notify_mail_subject', 'Subject'),
        (15, 'notify_mail_msg_1', ''),
        (16, 'notify_mail_msg_2', ''),
        (17, 'notify_mail_msg_3', ''),
        (18, 'notify_mail_msg_4', '');


