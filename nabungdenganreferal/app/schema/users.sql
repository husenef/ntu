CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` tinytext COLLATE utf8_bin,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `name` tinytext COLLATE utf8_bin,
  `last_name` tinytext COLLATE utf8_bin,
  `mail` text COLLATE utf8_bin,
  `country` text COLLATE utf8_bin,
  `ewallet_id` int(11) DEFAULT NULL,
  `ewallet_login` text COLLATE utf8_bin,
  `user_login` tinytext COLLATE utf8_bin NOT NULL,
  `user_password` text COLLATE utf8_bin NOT NULL,
  `is_mail_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

