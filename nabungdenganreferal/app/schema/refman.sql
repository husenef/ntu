-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.8-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4482
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para refmanager_db
DROP DATABASE IF EXISTS `refmanager_db`;
CREATE DATABASE IF NOT EXISTS `refmanager_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `refmanager_db`;


-- Volcando estructura para tabla refmanager_db.brokers
DROP TABLE IF EXISTS `brokers`;
CREATE TABLE IF NOT EXISTS `brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` tinytext COLLATE utf8_bin,
  `domain` tinytext COLLATE utf8_bin,
  `ref_link` text COLLATE utf8_bin,
  `reg_val` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- Volcando datos para la tabla refmanager_db.brokers: ~2 rows (aproximadamente)
DELETE FROM `brokers`;
/*!40000 ALTER TABLE `brokers` DISABLE KEYS */;
INSERT INTO `brokers` (`id`, `company`, `domain`, `ref_link`, `reg_val`) VALUES
	(2, 'Empire Money', 'http://www.empiremoney.com', 'http://www.empiremoney.com/user?user=ASDAS67%iduser%CHK234C345346', 50),
	(8, 'Big money', 'http://bigmoney.com', 'http://bigmoney.com/user?user=ASD76SD7AF98C7C%iduser%M9879VBM987CBM', 50);
/*!40000 ALTER TABLE `brokers` ENABLE KEYS */;


-- Volcando estructura para tabla refmanager_db.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` tinytext COLLATE utf8_bin,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla refmanager_db.config: ~0 rows (aproximadamente)
DELETE FROM `config`;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;


-- Volcando estructura para tabla refmanager_db.ewallets
DROP TABLE IF EXISTS `ewallets`;
CREATE TABLE IF NOT EXISTS `ewallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ewallet_type` tinytext COLLATE utf8_bin,
  `ewallet_url` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- Volcando datos para la tabla refmanager_db.ewallets: ~3 rows (aproximadamente)
DELETE FROM `ewallets`;
/*!40000 ALTER TABLE `ewallets` DISABLE KEYS */;
INSERT INTO `ewallets` (`id`, `ewallet_type`, `ewallet_url`) VALUES
	(6, 'Neteller', 'http://www.neteller.com'),
	(7, 'Paypal', 'http://www.paypal.com'),
	(10, 'Skrill', 'http://www.skrill.com');
/*!40000 ALTER TABLE `ewallets` ENABLE KEYS */;


-- Volcando estructura para tabla refmanager_db.history
DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `broker_id` int(11) NOT NULL,
  `pay` float DEFAULT NULL,
  `is_complete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- Volcando datos para la tabla refmanager_db.history: ~12 rows (aproximadamente)
DELETE FROM `history`;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` (`id`, `date`, `user_id`, `broker_id`, `pay`, `is_complete`) VALUES
	(1, '0000-00-00', 5, 2, 50, 1),
	(2, '0000-00-00', 5, 2, 50, 1),
	(3, '0000-00-00', 2, 8, 50, 1),
	(4, '0000-00-00', 3, 8, 50, 1),
	(5, '0000-00-00', 4, 8, 50, 1),
	(6, '0000-00-00', 5, 8, 50, 1),
	(7, '0000-00-00', 5, 2, 50, 1),
	(8, '0000-00-00', 3, 2, 50, 0),
	(9, '0000-00-00', 2, 2, 50, 1),
	(10, '2014-02-05', 5, 2, 123, 0),
	(11, '2014-02-05', 4, 8, 12, 1),
	(12, '2014-02-05', 5, 2, 1200, 0);
/*!40000 ALTER TABLE `history` ENABLE KEYS */;


-- Volcando estructura para tabla refmanager_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` tinytext COLLATE utf8_bin,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `name` tinytext COLLATE utf8_bin,
  `last_name` tinytext COLLATE utf8_bin,
  `mail` text COLLATE utf8_bin,
  `country` text COLLATE utf8_bin,
  `ewallet_id` int(11) DEFAULT NULL,
  `ewallet_login` text COLLATE utf8_bin,
  `user_login` tinytext COLLATE utf8_bin NOT NULL,
  `user_password` text COLLATE utf8_bin NOT NULL,
  `is_mail_confirm` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla refmanager_db.users: ~9 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `code`, `role`, `name`, `last_name`, `mail`, `country`, `ewallet_id`, `ewallet_login`, `user_login`, `user_password`, `is_mail_confirm`) VALUES
	(1, '1234567', 1, 'Damian', 'Bartumeu', 'dbartumeu@gmail.com', 'Cuba', NULL, NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
	(2, 'd0c2d8', 0, 'John', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'jdoe', '21232f297a57a5a743894a0e4a801fc3', 0),
	(3, 'd0c2d9', 0, 'Pedro', 'Jose', 'pedro@gmail.com', 'España', 7, 'pedro@gmail.com', 'js', '21232f297a57a5a743894a0e4a801fc3', 0),
	(4, 'd0c3d8', 0, 'Raul', 'Doe', 'john@gmail.com', 'Cuba', 7, 'john@gmail.com', 'ra', '21232f297a57a5a743894a0e4a801fc3', 0),
	(5, 'd0c3d9', 0, 'Alfredo', 'Doe', 'al@gmail.com', 'Brasil', 7, 'john@gmail.com', 'al', '21232f297a57a5a743894a0e4a801fc3', 1),
	(6, 'd0v2d8', 0, 'Alberto', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'alb', '21232f297a57a5a743894a0e4a801fc3', 0),
	(7, 'd0q2d8', 0, 'Leonardo', 'Doe', 'john@gmail.com', 'Argentina', 7, 'john@gmail.com', 'leo', '21232f297a57a5a743894a0e4a801fc3', 0),
	(8, 'd0r2d8', 0, 'Ricardo', 'Doe', 'john@gmail.com', 'Cuba', 6, 'john@gmail.com', 'ric', '21232f297a57a5a743894a0e4a801fc3', 1),
	(9, 'd0a2d8', 0, 'John', 'Doe', 'john@gmail.com', 'Cuba', 10, 'john@gmail.com', 'j', '21232f297a57a5a743894a0e4a801fc3', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
