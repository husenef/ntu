-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.8-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.0.0.4482
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para refmanager_db
CREATE DATABASE IF NOT EXISTS `refmanager_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `refmanager_db`;


-- Volcando estructura para tabla refmanager_db.brokers
CREATE TABLE IF NOT EXISTS `brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` tinytext COLLATE utf8_bin,
  `domain` tinytext COLLATE utf8_bin,
  `ref_link` text COLLATE utf8_bin,
  `reg_val` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla refmanager_db.config
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` tinytext COLLATE utf8_bin,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla refmanager_db.ewallets
CREATE TABLE IF NOT EXISTS `ewallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ewallet_type` tinytext COLLATE utf8_bin,
  `ewallet_url` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla refmanager_db.history
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `broker_id` int(11) NOT NULL,
  `pay` float DEFAULT NULL,
  `is_complete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla refmanager_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` tinytext COLLATE utf8_bin,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `name` tinytext COLLATE utf8_bin,
  `last_name` tinytext COLLATE utf8_bin,
  `mail` text COLLATE utf8_bin,
  `country` text COLLATE utf8_bin,
  `ewallet_id` int(11) DEFAULT NULL,
  `ewallet_login` text COLLATE utf8_bin,
  `user_login` tinytext COLLATE utf8_bin NOT NULL,
  `user_password` text COLLATE utf8_bin NOT NULL,
  `is_mail_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
INSERT INTO `users` (`id`, `code`, `role`, `name`, `last_name`, `mail`, `country`, `ewallet_id`, `ewallet_login`, `user_login`, `user_password`, `is_mail_confirm`) VALUES
	(1, 'a0d1m3', 1, 'none', 'none', 'none', 'none', NULL, NULL, 'admin', '202cb962ac59075b964b07152d234b70', 1),
-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
