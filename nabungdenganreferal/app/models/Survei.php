<?php

namespace models;

class Survei extends \DB\SQL\Mapper {

    //put your code here

    function __construct() {
        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'survei');
    }

    function save_survei($data, $cab) {
        date_default_timezone_set('Asia/Jakarta');
        $this->id_link = $data['link_id'];
        if (!empty($data['Produk']))
            $this->id_produk = $data['Produk'];
        $this->id_agen = $data['ref_id'];
        $this->id_cabang = $data['cabang'];
        $this->id_user_cabang = $cab;
        $this->nama = $data['nama_client'];
        $this->alamat = $data['alamat_client'];
        $this->ktp = $data['ktp_client'];
        $this->telephone = $data['telephone_client'];
        $this->jenis_produk = $data['jenis'];
        $this->status = 1;
        if (!empty($data['dana']))
            $this->dana = $data['dana'];
        $this->others_merek = (!empty($data['others_merek'])) ? $data['others_merek'] : '';
        $this->others_produk = (!empty($data['others_produk'])) ? $data['others_produk'] : '';
        if (!empty($data['tahun']))
            $this->tahun = $data['tahun'];
        $this->date_save = date('Y-m-d H:i:s');

        $this->used = ($data['used'] == 1) ? 2 : 1;
        $this->more_text = $data['more'];
        return $this->save();
    }

    function get_survei($fil_col, $fil_data, $options = null) {
        return $this->find(array($fil_col . '=?', $fil_data), $options);
    }

    function get_data($id) {
        $tmp = $this->load(array('id=?', $id));
        $tmp->cast();
        if ($tmp)
            return $tmp;
        else
            return null;
    }

    function get_by_id_click($id, $field) {
        $tmp = $this->load(array('id_link=? and status=3', $id));
        if ($tmp)
            return $tmp[$field];
        else {
            return 0;
        }
    }

    function get_count_data($where = null) {
        return $this->count($where);
    }

    function find_survei_active($find, $val) {
        $tmp = $this->find(array("$find=? and status=?", $val, '3'));
        return $tmp;
    }

    function update_status($id, array $r = null) {
        $this->load(array('id=?', $id));
        $this->status = $r['code'];
        $this->extra_text = (isset($r['extra_text'])) ? $r['extra_text'] : '';
        $this->save();
    }

    function get_surveis($search = null, $option = null) {
        $tmp = $this->find($search, $option);
        return $tmp;
    }

    function survei_reset() {
        return $this->reset();
    }

    function survei_all($where = array(), $order = array(), $ttl = 0) {
//        $where = ($where == '') ? array() : $where;
//        $order = ($order == '') ? array() : $order;
        return $this->find($where, $order, $ttl);
    }

    function update_delete($id, $code) {
        $this->load(array('id=? and enval=?', $id, 'A'));
        $this->enval = $code;
        $this->save();
    }

    function select_s($fields = '*', $filter = null, $options = array(), $ttl = 0) {
        return $this->select($fields, $filter, $options, $ttl);
    }

    function execs() {
        return $this->db->exec();
    }

}
