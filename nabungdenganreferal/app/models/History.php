<?php

namespace models;

class History extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'history');
    }

    function get_history($user = null, $options = null) {
        if (!isset($user))
            return $this->find(null, $options);
        else
            return $this->find(array('user_id=?', $user), $options);
    }

    function get_reg_confirmed_counts() {
        return $this->count(array('status = ? OR status = ?', 4, 8));
    }

    function get_reg_pend_counts() {
        return $this->count(array('status = ? OR status = ? OR status = ? OR status = ? OR status = ? OR status = ?', 1, 2, 3, 5, 6, 7));
    }

    function get_total_regs_val($user = null) {

        if (!isset($user)) {
            $T = $this->select('SUM(pay) AS total', array('status =? OR status = ?', 4, 8));
        } else {
            $T = $this->select('SUM(pay) AS total', array('status =? OR status = ? AND user_id=?', 4, 8, $user));
        }

        if (isset($T[0]) && $T[0]['total'] > 0)
            return $T[0]['total'];
        else {
            return 0;
        }
    }

    function get_regs_field_value($filter_column, $filter_data, $return_value) {
        $tmp = $this->load(array($filter_column . '=?', $filter_data));

        if ($tmp) {
            $tmp->cast();
            return $tmp[$return_value];
        } else {
            return null;
        }
    }

    function get_regs_counts($user = null) {
        if (!isset($user))
            return $this->count();
        else
            return $this->count(array('user_id=?', $user));
    }

    function get_regs_by_broker_counts($broker) {
        return $this->count(array('broker_id=?', $broker));
    }

    function add_confirmation($data) {
        $obj_user = new \models\Users;

        $this->date = date('Y-m-d');
        $this->user_id = $obj_user->get_user('code', $data['usercode'], 'id');
        $this->broker_id = $data['broker_id'];
        $this->status = $data['status'];
        $this->pay = $data['pay'];

        $this->save();
    }

    function upd_confirmation($id, $data) {
        $obj_user = new \models\Users;
        $this->load(array('id=?', $id));

        $this->date = date('Y-m-d');
        $this->user_id = $obj_user->get_user('code', $data['usercode'], 'id');
        $this->broker_id = $data['broker_id'];
        $this->is_complete = $data['is_complete'];
        $this->pay = $data['pay'];

        $this->save();
    }

    function del_confirmation($id) {
        $this->load(array('id = ?', $id));
        $this->erase();
    }

    function complete_confirmation($id, $data) {
        $h = $this->load(array('id=?', $id));

        $this->status = $data;

        $this->save();
    }

}
