<?php

/**
 * Description of status
 *
 * @author damian
 */

namespace models;

class status extends \DB\SQL\Mapper {

    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'status');
    }

    function get_status() {
        $f3 = \Base::instance();
        return $this->find(array("lang=?",$f3->get('LANGUAGE')));
    }

    function get_status_field_value($filter_column, $filter_data, $return_value) {
        $f3 = \Base::instance();
        $tmp = $this->load(array($filter_column . '=? AND lang=?' , $filter_data,$f3->get('LANGUAGE')));

        if ($tmp) {
            $tmp->cast();
            return $tmp[$return_value];
        } else {
            return null;
        }
    }
    
    function upd_status($id) {
        $this->load(array('id=?', $id));
        
        // code here
        
        $this->save();
    }
    
    function del_status($id) {
        $this->load(array('id=?', $id));
        
        // code here
        
        $this->erase();
    }
    
}