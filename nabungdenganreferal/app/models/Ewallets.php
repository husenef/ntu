<?php

namespace models;

class Ewallets extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'ewallets');
    }

    function get_ewallets() {
        return $this->find();
    }

    function get_ewallet($id, $value) {
        $tmp = $this->load(array('id=?', $id));
        $tmp->cast();
        return $tmp[$value];
    }

    function get_ewallet_type($id) {
        $etype = $this->load(array('id = ?', $id));
        return $etype->ewallet_type;
    }

    function add_ewallet($data) {
        $this->ewallet_type = $data['ewallet'];
        $this->ewallet_url = $data['ewallet_link'];
        $this->save();
    }

    function upd_ewallet($id, $data) {
        $this->load(array('id=?', $id));

        $this->ewallet_type = $data['ewallet'];
        $this->ewallet_url = $data['ewallet_link'];
        $this->save();
    }

    function del_ewallet($id) {
        $this->load(array('id = ?', $id));
        $this->erase();
    }

}
