<?php

namespace models;

class Kategori extends \DB\SQL\Mapper {

    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'produk_kategori');
    }

    function get_kategori() {
        return $this->find(' parent_cat = 0 ');
    }

    function get_child($id_parent) {
        $tmp = $this->find(" parent_cat = $id_parent ");
        return $tmp;
    }

    function get_child_count($id_parent) {
        return $this->count(array('parent_cat=?', $id_parent));
    }

    function upd_kategori($id, $data) {
        $this->load(array('id_cat=?', $id));

        $this->nama = $data['nama_kategori'];
//        $this->parent_cat = $data['parent_id'];
//        $this->jenis = $data['jenis'];
        $this->save();
    }

    function add_kategori($data) {
        $this->nama = $data['nama_kategori'];
//        $this->parent_cat = $data['parent_id'];
//        $this->jenis = $data['jenis'];
        $this->save();
    }

    function del_cat($id) {
        $this->load(array('id_cat = ?', $id));
        $this->erase();
    }

    function get_cat() {
        $tmp = $this->find(null, array('order' => 'parent_cat'));
        return $tmp;
    }

    function get_kategori_field($id, $field) {
//        $tmp = $this->load(array('id_cat = ?', $id));
//        $this->cast($tmp);
//        return $tmp;
        $tmp = $this->load(array('id_cat=?', $id));
        $tmp->cast();
        return $tmp[$field];
    }

    function reset() {
        parent::reset();
    }

    function get_jenis($code) {
        switch ($code) {
            case 1:
                $j = 'Mobil baru';
                break;
            case 2:
                $j = 'Mobil bekas';
                break;
            case 3:
                $j = 'Mobil maxi';
                break;
            case 4:
                $j = 'Motor baru';
                break;
            case 5:
                $j = 'Motor bekas';
                break;
            case 6:
                $j = 'Motor maxi';
                break;
        }
        return $j;
    }

}
