<?php

namespace models;

class Brokers extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'brokers');
    }

    function get_brokers() {
        return $this->find();
    }

    function add_broker($data) {
        $this->company = $data['company'];
        $this->desc = $data['desc'];
        $this->ref_link = $data['ref_link'];
        $this->reg_val = $data['reg_val'];
        $this->save();
    }

    function upd_broker($id, $data) {
        $this->load(array('id=?', $id));

        $this->company = $data['company'];
        $this->desc = $data['desc'];
        $this->ref_link = $data['ref_link'];
        $this->reg_val = $data['reg_val'];
        $this->save();
    }

    function del_broker($id) {
        $this->load(array('id = ?', $id));
        $this->erase();
    }

    function get_broker($id, $value) {
        $tmp = $this->load(array('id=?', $id));
        $tmp->cast();
        return $tmp[$value];
    }

    function get_brokers_counts() {
        return $this->count();
    }

}
