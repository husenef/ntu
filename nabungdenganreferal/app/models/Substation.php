<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//namespace models;
//
//class config extends \DB\SQL\Mapper {

namespace models;

class Substation extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'substation');
    }

    function get_substations($search = null, $option = null) {
        return $this->find($search,$option);
    }

    function add_substation($data) {
        $this->name = $data['name'];
//        $this->alamat = $data['address'];
//        $this->kota = $data['kota'];
        $this->telephone = $data['telephone'];
//        $this->karyawan = $data['karyawan'];
        $this->kode = $data['kode'];
        $this->area = $data['area'];
        $this->save();
    }

    function upd_substation($id, $data) {
        $this->load(array('id=?', $id));

        $this->name = $data['name'];
//        $this->alamat = $data['address'];
//        $this->kota = $data['kota'];
        $this->telephone = $data['telephone'];
//        $this->karyawan = $data['karyawan'];
        $this->kode = $data['kode'];
        $this->area = $data['area'];
        $this->save();
    }

    function del_substation($id) {
        $this->load(array('id = ?', $id));
        return $this->erase();
    }

    function get_substation($id, $find) {

        $tmp = $this->load(array('id=?', $id));
//        $tmp->cast();
        if ($find != null)
            return $tmp[$find];
        else
            return $tmp;
    }

    function get_substation_by_area($areaID) {
        $tmp = $this->find(array('area=?', $areaID));
        return $tmp;
    }

}
