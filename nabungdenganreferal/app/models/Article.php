<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Article
 *
 * @author husen
 */

namespace models;

class Article extends \DB\SQL\Mapper {

    //put your code here
    function __construct() {
        $f3 = \Base::instance();
        $db = $f3->get('DB');
        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'article');
    }

    function get_article($fileter = null) {
        return $this->find($fileter);
    }

    function get_count($where = null) {
        return $this->count($where);
    }

    function get_article_by_id($id) {
        $tmp = $this->load(array('id=?', $id));
        return $tmp;
    }

    function add_article($data = array()) {
        $this->judul = $data['judul'];
        $this->content = $data['content'];
        if (!empty($data['foto']))
            $this->image = $data['foto'];
        $this->author = $data['author'];
        if (!empty($data['link']))
            $this->link = $data['link'];
        $this->slug = $data['slug'];
        if (!empty($data['is_featured']))
            $this->is_featured = $data['is_featured'];
        $this->lokasi = $data['lokasi'];
        $this->date_save = date('Y-m-d H:i:s');
        $this->save();
    }

    function update_article($id, $data) {
        $this->load(array('id=?', $id));
        $this->judul = $data['judul'];
        $this->content = $data['content'];
        $this->slug = $data['slug'];
        $this->lokasi = $data['lokasi'];
        if (!empty($data['link']))
            $this->link = $data['link'];
        $this->is_featured = $data['is_featured'];
        if (!empty($data['foto']))
            $this->image = $data['foto'];
        $this->save();
    }

    function del_article($id) {
        $this->load(array('id = ?', $id));
        $this->erase();
    }

    function update_link($link) {
        $this->load(array('id = ?', $link));
        $this->link = '0';
        $this->save();
    }

    function get_link_name($link) {
        switch ($link) {
            case 1:
                $s = 'Welcome page';
                break;
            case 2:
                $s = 'Kebijakan & aturan';
                break;
            case 3:
                $s = 'FAQ';
                break;
            case 4:
                $s = 'Ketentuan Pengguna';
                break;
            case 5:
                $s = 'Hubungi Kami';
                break;
            default :
                $s = '';
                break;
        }
        return $s;
    }

    function parser_url($id) {
        switch ($id) {
            case 2:
                $url = 'kebijakan-aturan';
                break;
            case 3:
                $url = 'faq';
                break;
            case 4:
                $url = 'ketentuan-aturan';
                break;
            case 5:
                $url = 'hubungi-kami';
                break;
            default:
                $url = '';
                break;
        }
        return '/view/' . $url;
    }

    function lokasi_link($code) {
        switch ($code) {
            case 1:
                $n = 'Header';
                break;
            case 2:
                $n = 'Footer';
                break;
            case 3:
                $n = 'Sidebar';
                break;
            case 4:
                $n = 'Featured';
                break;
            default :
                $n = '-';
                break;
        }
        return $n;
    }

    function generate_lokasi($code) {
        switch ($code) {
            case 1:
                $n = 'Agen';
                break;
            case 2:
                $n = 'Cabang';
                break;
            default :
                $n = '-';
                break;
        }
        return $n;
    }

    function reset() {
        parent::reset();
    }

    function update_f($id) {
        $count = $this->count(array('id=?', $id));
        if ($count):
            $this->load(array('id=?', $id));
            $this->is_featured = 0;
            $this->link = 0;
            $this->save();
        else:
            return '';
        endif;
    }

    function get_article_custom($fil_col, $fil_data, $ret_value = null) {
        $count = $this->count(array($fil_col . '=?', $fil_data));
        if ($count):
            $tmp = $this->load(array($fil_col . '=?', $fil_data));

            if ($ret_value == null) {
                return $tmp;
            } else {
                return $tmp[$ret_value];
            }
        else:
            return $count;
        endif;
    }

    function get_article_by_link_id($link) {
        return $this->load(array('link=?', $link));
    }

}
