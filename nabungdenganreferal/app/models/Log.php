<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace models;

class Log extends \DB\SQL\Mapper {

    function __construct() {
        $f3 = \Base::instance();
        $db = $f3->get('DB');
        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'logs');
    }

    function add_logs($data) {
        $this->id_user = $data['id_user'];
        $this->ip = $data['ip'];
        $this->time_log = $data['time'];
        $this->browser = $data['browser'];
        $this->save();
    }

    function find_log($filter = NULL, array $options = NULL, $ttl = 0) {
        return $this->find($filter, $options, $ttl);
    }

    function select_logs($fields, $filter = NULL, array $options = NULL, $ttl = 0) {
        return $this->select($fields, $filter = NULL, $options = NULL, $ttl = 0);
    }

    function query_search($limit = null,$offset = 0) {
        $q = "SELECT * FROM ";
        $q .="(SELECT b.id, b.id_user, b.ip, b.time_log, b.browser FROM `logs` as b ORDER BY time_log desc ) as temp";
        $q .=" GROUP BY id_user ";
        $q .="ORDER BY id DESC ";
        if ($limit != null)
            $q .="LIMIT $offset, $limit";
        return $q;
    }

    function count_data() {
        $q = $this->query_search();
        $r = $this->db->exec($q);
        return count($r);
    }

    function browserName($agent) {
        if (!empty($agent)) {
            if (stripos($agent, 'Firefox') !== false) {
                $agent = 'firefox';
            } elseif (stripos($agent, 'MSIE') !== false) {
                $agent = 'ie';
            } elseif (stripos($agent, 'iPad') !== false) {
                $agent = 'ipad';
            } elseif (stripos($agent, 'Android') !== false) {
                $agent = 'android';
            } elseif (stripos($agent, 'Chrome') !== false) {
                $agent = 'chrome';
            } elseif (stripos($agent, 'Safari') !== false) {
                $agent = 'safari';
            } elseif (stripos($agent, 'AIR') !== false) {
                $agent = 'air';
            } elseif (stripos($agent, 'Fluid') !== false) {
                $agent = 'fluid';
            }
        }

        return $agent;
    }

}
