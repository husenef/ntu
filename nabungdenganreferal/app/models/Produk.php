<?php

namespace models;

class Produk extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'produk');
    }

    function get_produks($where = null, $option = null) {
        return $this->find($where, $option);
    }

    function add_produk($data) {
        $this->produk = $data['produk'];
        $this->desc = $data['desc'];
//        $this->ref_link = $data['ref_link'];
//        $this->reg_val = $data['reg_val'];
        $this->foto = $data['foto'];
        $this->merek = $data['merek'];
        $this->jenis = implode(',', $data['jenis']);
        if ($this->jenis == 'dana')
            $this->nilai = $data['nilai'];

        $this->save();
    }

//    function upd_broker($id, $data) {
//        $this->load(array('id=?', $id));
//
//        $this->company = $data['company'];
//        $this->desc = $data['desc'];
//        $this->ref_link = $data['ref_link'];
//        $this->reg_val = $data['reg_val'];
//        $this->save();
//    }
//
    function del_produk($id) {
        $this->load(array('id = ?', $id));
        $this->active = 0;
        $this->save();
//        $this->erase();
    }

//
    function get_produk($id, $value = null) {
        $tmp = $this->load(array('id=?', $id));
//        $tmp->cast();
        if ($value != null)
            return $tmp[$value];
        else
            return $tmp;
    }

    function get_produk_counts() {
        return $this->count(array('active=?', '1'));
    }

    function get_produk_by_jenis_merek($jenis, $merek) {
        $tmp = $this->find(array("merek=? and jenis LIKE ? and active=?", $merek, "%$jenis%", '1'));
        return $tmp;
    }

    function get_merek($id) {
        $tmp = $this->find(array("`jenis` LIKE ?", "%$id%"),array('group'=>'merek'));
        return $tmp;
    }

}
