<?php

namespace models;

class Users extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {
        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'users');
    }

    function add_user($data) {
        $this->code = $data['code'];
        $this->name = $data['name'];
        $this->last_name = $data['last_name'];
        $this->mail = $data['mail'];
        $this->user_login = $data['user_login'];
        $this->user_password = $data['user_password'];
        $this->ewallet_login = $data['ewallet_login'];
        $this->date = date('Y-m-d');
        $this->telephone = $data['telephone'];
        $this->address = $data['address'];
        if (!empty($data['role'])) {
            if ($data['role'] == 2) {
                $this->cabang = $data['cabang'];
                $this->role = 2;
                $this->nik = $data['nik'];
                $this->status = $data['status'];
            }
        }
        $this->is_mail_confirm = ($data['mail_confirm'] == 1) ? 1 : 0;
        $this->active = ($data['active'] == 1) ? 1 : 0;
        $this->ewallet_type = $data['ewallet_type'];
        if (!empty($data['is_mail_confirm']))
            $this->is_mail_confirm = $data['is_mail_confirm'];
        $this->save();
    }

    function upd_user($id, $data) {
        $this->load(array('id = ?', $id));
        $this->name = $data['name'];
        $this->last_name = $data['last_name'];
        $this->mail = $data['mail'];
        $this->telephone = $data['telephone'];
        if (!empty($data['ewallet_login']))
            $this->ewallet_login = $data['ewallet_login'];
        $this->address = $data['address'];
        if (!empty($data['role']) && $data['role'] == 2) {
            $this->cabang = $data['cabang'];
            $this->nik = $data['nik'];
            $this->status = $data['status'];
        }
        if (isset($data['user_login']))
            $this->user_login = $data['user_login'];
        if (!empty($data['img']))
            $this->img = $data['img'];
        if (!empty($data['user_password']))
            $this->user_password = md5($data['user_password']);
        $this->save();
    }

    function del_user($id) {
        $this->load(array('id = ?', $id));
        $this->active = 0;
        $this->save();
//        $obj_his = new \models\History;
//        $uhis = $obj_his->get_history($id);
//        foreach ($uhis as $h) {
//            $obj_his->del_confirmation($h->id);
//        }
//        $this->erase();
    }

    function upd_admin_pass($id, $pass) {
        $this->load(array('id = ?', $id));
        $this->user_password = md5($pass);
        $this->save();
    }

    function confirm_user($user) {
//        $this->load(array('user_login = ?', $user));
        $this->load(array('id = ?', $user));
        $this->is_mail_confirm = 1;

        $this->update();
    }

    function get_users($options = null) {
        return $this->find(array('role=?', 0), $options);
    }

//    function get_users_by_role($col, $find, $options = null) {
//        return $this->find(array("$col=?", $find), $options);
//    }
    function get_users_by_role($find = array(), array $options = null) {
        return $this->find($find, $options);
    }

    function get_user_select($fields, $filter, $options, $ttl) {
        return $this->select($fields, $filter, $options, $ttl);
    }

    function get_user($fil_col, $fil_data, $ret_value) {
        $tmp = $this->load(array($fil_col . '=?', $fil_data));

        if ($tmp) {
            $tmp->cast();
            return $tmp[$ret_value];
        } else {
            return null;
        }
    }

    function get_user_lost_pass($name, $lastname, $mail) {
        $tmp = $this->load(array('name=? AND last_name=? AND mail=?', $name, $lastname, $mail));

        if ($tmp) {
            $this->user_password = md5($tmp->code);
            $this->save();
            return $tmp;
        } else {
            return false;
        }
    }

    function get_users_counts() {
        return $this->count(array('role=?', 0));
    }

    function get_users_confirmed_counts() {
        return $this->count(array('role=? AND is_mail_confirm = ?', 0, 1));
    }

    function get_users_pend_counts() {
        return $this->count(array('role=? AND is_mail_confirm = ?', 0, 0));
    }

    function get_ewallet_users_count($ewallet_id) {
        return $this->count(array('ewallet_id=?', $ewallet_id));
    }

    function get_user_by_id($id) {
        $tmp = $this->load(array('id=?', $id));
        if ($this->count(' id=' . $id)) {
            return $tmp;
        } else {
            return null;
        }
    }

    function checklogin() {
        return (($this->f3->get('SESSION.user')) && ($this->f3->get('SESSION.user.is_mail_confirm'))) ? TRUE : FALSE;
    }

    function get_user_by_va($va) {
        $tmp = $this->load(array('ewallet_login=? and is_mail_confirm=?', $va, 1));
        return $tmp;
    }

    function users_s($fields = '*', $filter = null, $options = array(), $ttl = 0) {
        return $this->select($fields, $filter, $options, $ttl);
    }

    function user_by_load($filter = NULL, array $options = NULL) {
        return $this->load($filter, $options);
    }

    function get_user_by_query($stingsearch = array(), $option = null) {
        return $this->find($stingsearch, $option);
    }

    function get_user_by_query_count($c = '') {
        return $this->count($c);
    }
    function reset() {
        parent::reset();
    }

}
