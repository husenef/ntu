<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area
 *
 * @author husen
 */

namespace models;

class Area extends \DB\SQL\Mapper {

    //put your code here
    // Instantiate mapper
    function __construct() {
        $f3 = \Base::instance();
        $db = $f3->get('DB');
        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'area');
    }

    function get_areas() {
        return $this->find();
    }

    function add_area($data) {
        $this->nama = $data['nama'];
        $this->save();
    }

    function update_area($data) {
        $this->load(array('id=?', $data['id']));
        $this->nama = $data['nama'];
        $this->save();
    }

    function delet($id) {
        $this->load(array('id=?', $id));
        $this->erase();
    }

    function get_area($search = null, $option = null) {
        $tm = $this->load($search, $option);
        if ($tm)
            return $tm;
        else
            return null;
    }

}
