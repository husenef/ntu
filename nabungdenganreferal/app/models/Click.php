<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of click
 *
 * @author Agus
 */

namespace models;

class Click extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'click');
    }

    function add_click($data) {
        $this->user_id = $data['user_id'];
        $this->email_nasabah = $data['email_nasabah'];
        $this->date_share = date('Y-m-d H:i:s');
        $this->save();
        return $this->last();
    }

    /* cek data user+email adata atau tidak */

    function check_count($search = array()) {
        $tmp = $this->count(array('user_id= ? AND email_nasabah= ?', $search['user_id'], $search['email_nasabah']));
        return ($tmp);
    }

    /* quer chek data nasabah+agen ada atau tidak */

    function get_data($find) {
        $tmp = $this->load(array('user_id= ? AND email_nasabah= ?', $find['user_id'], $find['email_nasabah']));
//        $tmp->cast();
        return $tmp;
    }

    function get_data_by_user($id) {
        $tmp = $this->find(array('user_id=?', $id));
        return $tmp;
    }

    function update_count($id, $newcount) {
        $this->load(array('id= ? ', $id));
        $this->clicked = $newcount;
        $this->save();
    }

    function cari($id) {
        $tmp = $this->load(array('id=?', $id));
        if ($tmp)
            return $tmp;
        else
            return 0;
    }

    function get_data_custom($id, $value) {
        $tmp = $this->load(array('id=?', $id));
//        $tmp->cast();
        return $tmp[$value];
    }

    /* reset query */

    function reset() {
        parent::reset();
    }

}
