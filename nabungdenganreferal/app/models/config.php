<?php

/**
 * Description of config
 *
 * @author damian
 */

namespace models;

class config extends \DB\SQL\Mapper {

    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'config');
    }

    function get_config() {
        return $this->find();
    }

    function get_config_field_value($filter_column, $filter_data, $return_value) {
        $tmp = $this->load(array($filter_column . '=?', $filter_data));

        if ($tmp) {
            $tmp->cast();
            return $tmp[$return_value];
        } else {
            return null;
        }
    }
    
    function upd_config($key,$data) {
        $this->load(array('value=?', $key));
        
        $this->data = $data;
        
        $this->save();
        $this->reset();
    }
    
    function del_config($id) {
        $this->load(array('id=?', $id));
        
        // code here
        
        $this->erase();
    }
    
}