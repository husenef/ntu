<?php

namespace models;

class Comments extends \DB\SQL\Mapper {

    // Instantiate mapper
    function __construct() {

        $f3 = \Base::instance();
        $db = $f3->get('DB');

        // This is where the mapper and DB structure synchronization occurs
        parent::__construct($db, 'ec_comments');
    }

    function countByContainer($container) {

        return $this->count(array('container = ?', $container));
    }

    function listByContainer($container) {

        $result = array();
        $comments = $this->select('*', array('container = ? AND parent = ?', $container, 0));

        foreach ($comments as $key => $comment) {
            $result[$key] = $comment->cast();
            $result[$key]['replies'] = $this->getReplies($comment->id);
        }

        return $result;
    }

    function getReplies($comment) {

        return $this->select('*', array('parent = ?', $comment));
    }

    function last($limit = 5) {
        $result = array();
        $comments = $this->find(NULL, array('order' => 'last_modified DESC', 'limit' => $limit));

        $containers = new Containers;

        foreach ($comments as $key => $comment) {
            $result[$key] = $comment->cast();
            $container = $containers->findone(array('id=?', $comment->container))->name;
            $result[$key]['container'] = $container;
        }

        return $result;
    }

    function saveComment($post) {

        $this->reset();

        $this->container = $post['container'];
        $this->email = $post['email'];
        $this->name = $post['name'];
        ;
        $this->comment = nl2br($post['comment']);
        $this->parent = $post['parent'];
        $this->last_modified = date("Y-m-d H:i:s");

        $this->save();

        // only for new comments
        $result = $this->cast();
        $result['replies'] = array();

        return $result;
    }

    function eraseComment($id) {

        $this->load(array('id=?', $id));
        $this->erase();

        $this->reset();

        $childrens = $this->find(array('parent=?', $id));
        foreach ($childrens as $children) {
            $this->reset();
            $this->load(array('id=?', $children->id));
            $this->erase();
        }
    }

}
