jQuery(document).ready(function () {
    $('#accordion').accordion();


    //main menu
    var nav = 'mobile';
    $('.mobile-nav').on('click', function () {
        if (nav == 'mobile') {
            $('.dekstop-nav').fadeIn();
            nav = 'desktop';
        } else {
            $('.dekstop-nav').fadeOut();
            nav = 'mobile';
        }
    });
    $(window).resize(function () {
        if ($(window).width() > 798) {
            $('.dekstop-nav').css({'display': 'block'});
        } else {
            $('.dekstop-nav').css({'display': 'none'});
        }
    });
    $('.account').on('click', function () {
        if ($(this).val() == 'yes') {
            $('#adira').show().find('input').removeAttr('disabled');
        } else {
            $('#adira').hide().find('input').attr('disabled', 'disabled');
        }
    });

    $('#register').validate({
        rules: {
            name: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true
            },
            address: {
                required: true
            },
            username: {
                required: true,
                minlength: 5
            },
            pass: {
                required: true,
                minlength: 8
            }
        },
        messages: {
            name: "Masukkan nama pertama",
            lastname: "Masukkan nama terahir",
            email: {
                required: 'Masukkan email anda',
                email: "Masukkan valid email"
            },
            phone: {
                required: "Masukkan No Telepon anda",
                number: "Masukkan angka"
//                phone:"Masukkan valid phone"
            },
            address: "Masukkan alamat Anda",
        }
    });

    $('.popup').magnificPopup({
        type: 'image'
    });

    if ($('#admin_date').size() > 0) {
        $('#admin_date').datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: "d"
        });
    }
});