$(document).ready(function() {
    $('#link-ewallet').attr('href', $('#sel-ewallet').find(':selected').data('url'));
    $('#sel-ewallet').change(function() {
        //console.log($(this).find(':selected').data('url'))
        $('#link-ewallet').attr('href', $(this).find(':selected').data('url'));
    });


    $('#drop-user').on('show.bs.dropdown', function() {
        $('.dropdown-menu').addClass('fadeInDown');
        $('.dropdown-menu').removeClass('fadeOutUp');
    });
    $('#drop-user').on('hide.bs.dropdown', function(e) {

        e.preventDefault();

        $('.dropdown-menu').removeClass('fadeInDown');
        $('.dropdown-menu').addClass('fadeOutUp');
        $('#drop-user').removeClass('open');

    });

    $("[data-toggle=popover]").popover({
        trigger: "hover",
        container: '#ppanel',
        html: "true"
    });

    $(".trigger-tip").click(function(e) {
        e.preventDefault();
    });

    $('.sel-status').change(function() {
        var target = $(this).data('target');
        $('.check-his[value=' + target + ']').attr('checked', true);
    });

//    $('.img a').fancybox({
//        'transitionIn': 'elastic',
//        'transitionOut': 'elastic',
//    });
});

// Image Captcha
function imageCaptcha() {
    $('#reLoadMe').attr('src', 'image_captcha/' + Math.round(Math.random() * 1000));
}
// End Image Captcha


