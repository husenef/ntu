<?php

$f3 = require('app/lib/base.php');
//$counter = require('app/lib/counter.php');
$f3->set('DEBUG', 1);
// enviroment
$f3->config('app/config.ini');
// routing
$f3->config('app/routes.ini');

// databases
if ($f3->get('INSTALLED') == 'TRUE')
    $f3->set('DB', new DB\SQL("mysql:host=" . $f3->get('mysql_host') . ";port=" . $f3->get('mysql_port') . ";dbname=" . $f3->get('mysql_db'), $f3->get('mysql_user'), $f3->get('mysql_pass')));


// services
if ($f3->get('INSTALLED') == 'TRUE') {
//    $f3->set('obj_ewallets', new models\Ewallets);
//    $f3->set('obj_users', new models\Users());
//    $f3->set('obj_config', new models\config());
//    $f3->set('obj_produk', new models\Produk());
//    $f3->set('obj_kategori', new models\Kategori());
//    $f3->set('obj_cabang', new models\Substation());
//    $f3->set('obj_survei', new \models\Survei());
//    $f3->set('obj_click', new models\Click());
    $f3->set('obj_article', new models\Article());
//    $f3->set('c_survei', new controllers\Survei());
//    $f3->set('c_tools', new controllers\Tools());
//    $f3->set('obj_area', new \models\Area());

    /* plugin */
    $f3->set('plug_text', new Text());
    /* endak kepake */
    //$f3->set('obj_util', new controllers\Util());
}
$f3->set('ONERROR', function($f3) {
    echo \Template::instance()->render('error.htm');
});
// app
$f3->run();
